# Glossary 

Some terms in the docs might be unknown to you; this section should help you achieve a 
better understanding of these words and how they relate to Cardschat.

## CC 
**CardsChat** 

## CB 
**ClickyBounce** - [Link](https://cb.legendcorp.com/clickybounce/index.php)

## CL 
**CodeLibrary** - [Link](https://bitbucket.org/legendcorp/codelibrary/) 

## CLS
**Cumulative Layout Shift**: The amount that the page layout shifts during the loading phase. The score is rated 
from 0–1, where zero means no shifting and 1 means the most shifting. This is important 
because having pages elements shift while a user is trying to interact with it is a bad 
user experience. 

[Read More](https://web.dev/cls/)

## FID
**First Input Delay**: The time from when a user first interacts with your page (when they clicked a link, tapped 
on a button, and so on) to the time when the browser responds to that interaction. 
This measurement is taken from whatever interactive element that the user first clicks. 
This is important on pages where the user needs to do something, because this is when the 
page has become interactive.

[Read More](https://web.dev/fid/)

## LCP 
**Largest Contentful Paint**: The amount of time to render the largest content element visible in the viewport, 
from when the user requests the URL. The largest element is typically an image or video, 
or perhaps a large block-level text element. This is important because it tells the reader 
that the URL is actually loading. 

[Read More](https://web.dev/lcp/)