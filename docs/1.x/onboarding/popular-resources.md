# Popular Resources

## Confluence
- [iTech - ArchiTech Portal](https://confluence.jira.tech/display/PP/ArchiTech+Portal)
- [CardsChat - Tribe Information & Ways of Working](https://confluence.jira.tech/pages/viewpage.action?pageId=77466869)

## Talks
- [iTech - Show & Tell](https://confluence.jira.tech/pages/viewpage.action?pageId=80021889)
- [Engineering - Tech Talks](https://confluence.jira.tech/display/FED/Tech+Talks)

## Other
- [HiBob - for all your Holiday requesting & people spying needs](https://app.hibob.com/home)
- [The 'Spotify Model'](https://www.youtube.com/watch?v=4GK1NDTWbkY)