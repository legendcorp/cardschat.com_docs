# Tech

## Tools
The following are some of the Communication and Agile tools that we use.

You may need to request credentials / access. If so, speak to your manager about how to obtain these.

- Confluence
- Jira
- Bitbucket
    - See CC's list of repos [here](../../README.md#repositories).
    - There **will** be other repos. Check with your fellow Archs.
- Rocket.Chat

## Developer Tools
As an engineer, your environment is *your* environment. We don't have strong opinions on what you use but here are the very basics.

- **IDE** - PHPStorm, WebStorm, VSCode, Sublime, etc...
- **Command-line Tool** - Terminal, iTerm, Putty, etc...
- **Browser** - Chrome, FireFox, Safari, Opera, etc...

If there are tools you need, or you require a license, reach out to your manager. There's a chance we already have licenses available or they can be requested.

## Useful Tips & Tricks
### How to configure a global `.gitignore`
If every developer were to commit their own environment-specific set of `.gitignore` rules, we'd end up with a long list to maintain.

Multiply this by the number of projects a developer may touch, you'll potentially see the benefit of a personal, global `.gitignore`.

::: details Steps

#### 1. Create your `.gitignore`
First, you'll want to create the `.gitignore` file that will contain your global rules. The 'home' directory might be a good place to store it but choose wherever you're happy with.
```
touch ~/.gitignore
```
#### 2. Edit your `.gitignore`
Either open your file in a text editor or do so via the command-line with `vi`, `nano` etc.
What you place in here will obviously depend on your OS, IDE, and other factors.

For example, you may just wish to add the following if you're a Mac user using VSCode:
```
.DS_Store
.vscode
```
Or a Windows user with PHPStorm:
```
Thumbs.db
.idea
```
Perhaps this example from Octocat will inspire you: [octocat/.gitignore](https://gist.github.com/octocat/9257657)
#### 3. Configure `git` to use your `.gitignore`
This will update your `.gitconfig` and ensure that all projects adopt your personal `.gitignore`.

Be sure to update the path if you've added your file to somewhere other than the home directory.

For Mac users:
```
git config --global core.excludesfile ~/.gitignore
```
For Windows users:
```
git config --global core.excludesfile %USERPROFILE%\.gitignore
```

:::
