# Introduction

Welcome to [CC](../help/glossary.md#cc)!

It's possible that there are a few reasons as to why you may be here...

I'm going to assume that it's one of the following:
1. you've heard what a great tribe we are and have opted to join us
2. you've lucked out, and we scooped you up before anyone else!
3. you're a fellow Arch and just want to know a little more about us.

No matter which of these apply, if any, you're all welcome!

## Who are we and what is CardsChat?
### The product & brand

[CardsChat.com](https://www.cardschat.com/) was established in 2004.

We've grown to become **one of the internet's leading poker forums**, featuring **~4 million** posts by **~325k** valued members!

Our mission is to be the most helpful, informative, and friendly poker community online. We do this by providing users with access to a **poker forum** run by true poker enthusiasts, **poker strategy guides** for all types of players, **unbiased poker site reviews**, **exclusive freerolls and bonuses**, and much more.

Through doing this, we believe we can accomplish our vision of connecting to and helping every poker player around the world.

#### The past
Having been around for more than 16 years at the time of writing, CardsChat has established a strong presence in the world of online poker.

For an amazing look back at the **History of Online Poker & Poker Forums**, please do check out a talk CC's **Chad** gave on the subject.

##### A Brief History of Online Poker & Poker Forums

- Date: July 2020
- Duration: 33m
- Show & Tell Link: [Direct](https://confluence.jira.tech/pages/viewpage.action?pageId=80021889)
- Video Link: [Direct](https://zoom.us/rec/share/2JJaBejw2EhLcIHKrwbmB7YIL9_5X6a8gSJP__QMzR2Gz9ZR-c_Z9Q33bhHMGGMh?startTime=1596196738000)
    - Password: `Show&Tell@1t3ch`
- Deck Link: [Direct](https://confluence.jira.tech/download/attachments/80021889/poker%20history%20presentation.pdf?version=1&modificationDate=1596199423752&api=v2)

#### The present & future
For a brilliant summary from an engineering perspective on our present and future, check out this talk CC's **Roland** gave in a Tech Talk.

##### A summary of current things happening in Tribe tech and a look to the future.

<video width="680" height="400" controls>
  <source src="https://itechlondon-my.sharepoint.com/personal/hakim_miah_itech_media/Documents/videos/CardsChat%20-%20Roland.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- Date: 2nd October 2020
- Duration: 41m
- Video Link: [Direct](https://itechlondon-my.sharepoint.com/personal/hakim_miah_itech_media/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fhakim%5Fmiah%5Fitech%5Fmedia%2FDocuments%2Fvideos%2FCardsChat%20%2D%20Roland%2Emp4&parent=%2Fpersonal%2Fhakim%5Fmiah%5Fitech%5Fmedia%2FDocuments%2Fvideos&originalPath=aHR0cHM6Ly9pdGVjaGxvbmRvbi1teS5zaGFyZXBvaW50LmNvbS86djovZy9wZXJzb25hbC9oYWtpbV9taWFoX2l0ZWNoX21lZGlhL0VjTl8td3FMblVKSGtqOEgwWXpmaThnQk1DckdVNkNOS0o2c0syRFpiU3Q0RXc_cnRpbWU9SzFodFNHS04yRWc)

### The Tribe
iTech follow the ["Spotify Model"](https://www.youtube.com/watch?v=4GK1NDTWbkY) whereby we **CardsChat** is defined as a **Tribe**.

A Tribe consists of teams known as **Squads** and each squad will contain representatives from various **chapters**.

Chapters include *(but are not limited to)*:
- Content
- Conversion
- Delivery
- Design
- Engineering
- Forum
- Product
- SEO

Want to meet everyone?
[Meet the CC Tribe](https://confluence.jira.tech/display/CS/CC+Squads)

#### How we work
Everything you need to know about how we work can be found in our Confluence space: [Tribe Information & Ways of Working](https://confluence.jira.tech/pages/viewpage.action?pageId=77466869).

For more information on Ways of Working within the Engineering chapter of CC, head to our [Code of Conduct](../code-of-conduct.md) or [Development section](../development/coding-standards.md) in the Handbook.
