# Coding Standards

We abide by a set of Coding Standards and Guidelines as set out in the following repository: [Public-Code-Standards-and-Guides](https://bitbucket.org/legendcorp/public-code-standards-and-guides/src/master/)

As a general rule, just use common-sense. Look at the code around you, particulary more recent contributions from your fellow developers.

If you don't find something in the referenced broad guidelines, don't be shy, sound it out with the devs on RocketChat. They'll be glad to help.

## Quick Guide

### Standards for **Architecture**
* We should use dedicated folders to store styles and scripts for the pages. Keeping everything inside vBulletin WebTemplates will make everything very hard to maintain.
    * Transitioning to the new platform would require going through each page and checking for any inline styles and scripts.
* When creating **Plugins** we should use only `exported_plugins` directory and follow instructions from the readme file (`exported_plugins/README.md`).

For more information, see [Architecture](../architecture/high-level-schema.md).

### Standards for **Git**
See [Contribution Guidelines](./contribution-guidelines.md).

### Standards for **Accessibility**
* **Never** use `span`, `p`, `div` or similar elements to make interactive UI controls. **Always** use `a` or `button` for those. Generic inline or block elements don't have any default functionalities like keyboard support. Also, screen reader won't normally read those as links or buttons.
* **Always** provide text alternatives for UI controls with visual content like images or icons. Without this screen reader users will have no information about purpose of the element. This should be achieved by providing proper `aria-label` attribute on the element, or if applicable visual label.
* **Always** provide proper labels for form inputs. They should never be replaced by placeholder attribute.
* **Check** if the contrast of the page is sufficient. You can refer to this [contrast checker tool](https://webaim.org/resources/contrastchecker/). This will help not only visually impaired people, but all users.
* **Check** if all functionalities are accessible from keyboard.
* Avoid using the `tabindex` attribute with non-interactive elements to make something intended to be interactive focusable by keyboard input. Instead, use native HTML elements like `button` or `link`.

To learn more about **Web Accessibility** you can join our **#accessibility-guild** Rocket Chat channel.

### Standards for **HTML**
* When applicable, use proper **HTML5** tags like `main`, `section`, `article` or `nav`. Replacing those with generic `div` elements will affect various web related technologies, eg. RSS feed readers.
* Remember to keep proper heading hierarchy. This means don't skip heading levels and don't nest headings. Remember that document outline was never implemented.
* We should `lazyload` images whenever it's possible.
* We should use **relative path** for internal `scrollsmooth` in links. For example: `/page.php/#{id}` instead of `#{id}`.

If you are not sure what certain element does, [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML) is a great resource.

### Standards for **CSS**
* For all new implementations we should use [BEM naming methodology](https://cssguidelin.es/#bem-like-naming). This will ensure that all of our work will be maintainable and reusable.
* We should never style elements using tag names. This will make our CSS unmaintainable quickly, especially when we have multiple stylesheets and each of them provides different styles for example: `<a>` tags. We should always use the `class` attribute to style new elements.
* For setting `font-size` we should use relative units like `rem`. This will make implementing designs much more logical. This is also important for accessibility, as this will not override users preferred text sizes.
* We don't have to use CSS prefixes that much. Most of the popular features like `flexbox`, `border-radius` or `animation` will work fine with modern browsers. Styles added to the Component Library do not need prefixes because Webpack handles this on build.

### Standards for **JavaScript**
* For all new implementations we should stop using util libraries like **jQuery**, and when it's possible use vanilla **JavaScript**. This will make transition to new platform much easier, and the page will not be dependent on external libraries. This can also make the page a bit faster.
* Make sure that **JavaScript** features you are working on haven't been implemented before. Use existing functions if possible. This will prevent us from having parts of code that are doing the same thing.
* We should always leave comments describing all new functionalities. This will ensure that new developers will have some idea what certain parts are responsible for.
* Check if the functionality you are working on actually requires **JavaScript**. A lot of things can be done in pure **HTML** and **CSS**. For example: anchor links, smooth scroll.

### Standards for **PHP**
* If possible we should always put **PHP** code into the least amount of `<?php ?>` tags. For example:

**Good:**
``` php
<?php
    echo start_tag();
        echo post_content();
    echo end_tag();
?>
```
**Bad:**
``` php
<?php echo start_tag(); ?>
    <?php echo post_content(); ?>
<?php echo end_tag(); ?>
```
* We should always put semicolon at the end of each **PHP** tag.
* We should avoid rendering static **HTML** with `echo` methods. If it's necessary we should use `sprintf()` method. For example:

**Good:**
``` php
<div>
    <p>
        <?php echo post_content(); ?>
    </p>
</div>
```

``` php
<?php
    $content = post_content();
    echo sprintf('<div><p>%s</p></div>', $content);
?>
```

**Bad:**
``` php
<?php
    echo '<div>';
    echo '<p>';
    echo post_content();
    echo '</p>';
    echo '</div>';
?>
```

* We should take caution when using `isset()` in conditions. For example:
``` php
<?php
        $author = '';

        if(isset($author)) {
            echo sprintf('<div class="author"><p>%s</p></div>', $author);
        }
    ?>
```

* Above code will render empty div element. Using `!empty` in the condition would skip unnecessary rendering.
    * For **PHP** mixed with **HTML** we should use the following syntax in loops and conditions:
``` php
if(): endif;
```
``` php
<?php if(display_content()): ?>
    <div class="content">
    </div>
<?php endif; ?>
```
* For **PHP** without **HTML** in between we should use following syntax:
``` php
if() {}
```
``` php
<?php if(display_content()) {
    echo content();
} ?>
```

### Standards for **Wordpress Development**
* **Only** change the files that are inside `/wp-content directory`. You should never edit core wordpress files.
* **Never** change any files through admin panel.
* For most things we can use hooks. Don't hardcode any data that can be replaced with Filter or Action.
* Use Child Themes for any template changes. This would prevent from overriding main template with updates.
* Make sure that you can't use any of installed plugins functions before you install a new one. For example: we can replace most of our SEO plugins with just **Yoast Seo**.
* Remember to uninstall all plugins that you know won't be used anymore. This will prevent us from having lots of disabled plugins.
* Avoid using if statements for creating customized pages. Use Page Templates instead every time it's applicable.
* Remember to always describe what certain function in `functions.php` file does with a comment. This will help us keep the file clean and maintainable.

## Future Proofing
* We should remove all unnecessary and outdated libraries. A good example is [yahoo-dom-event.js](https://www.cardschat.com/clientscript/yui/yahoo-dom-event.js), which is 2009 library, and most of it's functions are now in native JS implementation.
* As agreed, we should drop support for IE and focus on Chrome, Firefox and Safari. Supporting Internet Explorer can introduce few problems with implementing polyfills:
    * Polyfills can be heavy which will cause longer loading time,
    * Supporting IE can force us to use outdated CSS rules and JS API's which can become difficult to maintain,
    * It can be very time consuming.
* Same goes for Microsoft Edge, the new Edge version is chromium-based, we shouldn't worry about supporting it.