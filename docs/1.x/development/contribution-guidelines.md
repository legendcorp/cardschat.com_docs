# Contribution Guidelines

We use [Git](https://git-scm.com/docs) as a method of version control, allowing us to have a record of each file that is changed. This is important as we have many developers working on one project at a time.

## Flow
We follow the Github Flow, Git Workflow which is a lightweight, branch-based workflow that supports teams and projects where deployments are made regularly. Walkthrough of GitHub Flow - [guides.github.com/introduction/flow/](https://guides.github.com/introduction/flow/).

## Git usage

### Branching

* Work from your own branch

* Your branch should follow the following branch naming: ```yourname_branchname-summary```
  * eg. ```jensonbutton_CCDB-1234-add-card-component```

* If you are branching off of `develop` in order to open a PR for the Dev Enivironment, add `--develop` to the end of your branch.
  * eg. ```jensonbutton_CCDB-1234-add-card-component--develop```
  * This allows you to create a clean branch name for your Production PR

* Update your branch often.
* Always do a `git status` before you add, and again before you commit.
* When pulling and pushing, always specify the remote location. Never just push and pull.
```
git push origin branchname
git pull origin master
```
* Push your local branch to the remote daily as a means of backup or for someone else to continue working from your branch if needed.
* Merge origin master into your local branch before creating a pull request.

### Commits

* Commit often - commit messages should be done on every commit, no matter how small the change was.
* Commit messages should be done on every commit, no matter how small of a commit it is.
* Commit messages should contain the Jira ticket ID and what was changed exactly. eg. which folder, page, file or section of the page was changed. This will make code review process much easier.
* Commit messages should be short and to the point, as well as being relevant to the work that is being done.
* Commit messages should be capitalised, and be in imperative tense.
* Commit message should start with a keyword describing what kind of update was implemented: eg. `ADD | CHANGE | FIX | REMOVE`

***Example:***
```
git commit -m "CHANGE: Alt text on HP corner image # CCO-123"
```

***Hints:***
* Use ```git add -p``` to show the line changes made in all files
* Use ```git add git.md -p``` to show the line changes made in the file `git.md`

## Things to never do
* **Never** use the `-a` flag or `.` when making a commit.
* **Never** use the `-f`, or `--force` flag when pushing to repo (unless in CodeLibrary).
* **Never** work directly from `master` no matter how small the change is.
* **Do not** use an alias that combines `add` and `commit` in one command.

**Bad**
```
git add .
git add -a
git commit -am
```

**Verrry Bad**
```
git add . -f
```

**Good!**
```
git add -p
git add Sources/sass/main.scss
```

## .gitignore
To ignore **CSS** and **JS** folders use the following:
```
js/*
css/*
!/Sources/js/*
!/Sources/css/*
```

### Pull Requests
Pull Requests (PR's) should be made for all merges.

A pull request should be created for all changes to the live site. This should be reviewed by at least **two** internal developers before you can merge.

Be considerate with the PR title and try to mirror the Jira Ticket title. The format should follow this pattern:

```
CCDB-123 # Create New Brazil Texas Hold'em page [PROD]
```
* Jira Ticket has been added - this automatically creates a link back to the ticket.
* Action and Summary of the work.
* [ENV] - Indicates that it's a PR in to the Dev Environment or Production.

Approvers are responsible for the code if it's pushed to master.

Feel free to also include your peers as this gives teams a good overview of things they might not have encountered before. It is there choice if they want to comment and approve unless specifically told to do so.

**Important:** Your pull request **must** to be approved before you can merge your work.

For further information, refer to this article about [good commit practices](https://chris.beams.io/posts/git-commit/).