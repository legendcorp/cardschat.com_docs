# About 

CardsChat is composed of five parts: 
- [News](/news/1.x/), powered by Wordpress;
- [CMS & Forum](/cms/1.x/), using vBulletin 3.8 with a custom build CMS (it'll become the CMS only);
- [Forum](/forum/1.x/), using Invision 4.5, currently being built (replacing the old);
- [Free Slots](/free-slots/1.x/), using Arcade Connector to communicate with the API;
- [Docs](/docs/1.x/), build with VuePress, optimized for writing technical documentation (this site);

## Diving In

If you’re new to CardsChat, I recommend starting with the [Code of Conduct](./code-of-conduct.md) and then the 
Onboarding section that is coming up right after this. The Development section contains everything that is required for 
you to start contributing. I would call attention to our [Coding Standards](./development/coding-standards.md) and the 
[Contribution Guidelines](./development/contribution-guidelines.md) pages.

More information about each individual project can be found on its own documentation area. It includes guidelines about 
setup and contribution, explanations, tutorials and etc...  

