# Code of Conduct

The CardsChat tribe is made up of a mixture of professionals working on every aspect of the mission.

Diversity is one of our strengths. With that, challenges will arise that can lead to communication issues and unhappiness. To that end, we have a few ground rules that we ask people to adhere to. This code applies equally to everyone.

This isn't an exhaustive list of things that you can't do. Rather, take it in the spirit in which it's intended - a guide to make it easier to enrich all of us and the technical communities in which we participate.

This code of conduct applies to all spaces used by the CardsChat tribe for communication. This includes RocketChat, Zoom, BitBucket, Confluence, meetups, conferences, and any other relevant forums.

## CC's Working Agreement
### Respect
- Everyone has an **Opinion**
- Everyone has **a Voice**
- Be **Mindful** of peoples time
- Be **Constructive and Respectful** with feedback

### Honesty
- Be **Open** and **Honest** with each other
- **Speak Up** - Don't be afraid
- Be Ethical - **Don't Lie**
- Don't take it **Personally**

### Trust
- **Trust** each other to do the **Best** you can
- Say what you will **Do** and **Do It**
- **Morality** & **Integrity**
- **Communicate** what you are doing
- **Trust** person ownership and be responsible

### Unity
- **Win as one**
- Do things **Together**
- **Lose as one** - learn from mistakes and improve
- Don't lay **Blame** at others
- **Push** each other to become better

### Helpfulness
- **Ask** questions
- **Willing** to assist with other tasks
- Go out of your way to **Help**
- **Help and Be Helped**
- Be **Approachable** and don't be afraid

## General Code of Conduct

- **Be friendly and patient.**
- **Be welcoming.** We strive to be a tribe that welcomes and supports people of all backgrounds and identities. This includes, but is not limited to members of any race, ethnicity, culture, national origin, colour, immigration status, social and economic class, educational level, sex, sexual orientation, gender identity and expression, age, size, family status, political belief, religion, and mental and physical ability.
- **Be considerate.** Your work will be used by other people, and you in turn will depend on the work of others. Any decision you take will affect users and colleagues, and you should take those consequences into account when making decisions.
- **Be respectful.** Not all of us will agree all the time, but disagreement is no excuse for poor behavior and poor manners. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. It's important to remember that a team where people feel uncomfortable or threatened is not a productive one. Members of the CardsChat tribe should be respectful when dealing with other members as well as with people outside the CardsChat tribe, colleagues and contractors.
- **Be careful in the words that you choose.** We are a team of professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put down other participants. Harassment and other exclusionary behavior aren't acceptable. This includes, but is not limited to:

  - Violent threats or language directed against another person.
  - Discriminatory jokes and language.
  - Exclusionary gendered language (guys, gentlemen, bros, etc).
  - Posting sexually explicit or violent material.
  - Posting (or threatening to post) other people's personally identifying information ("doxing").
  - Personal insults, especially those using racist or sexist terms.
  - Unwelcome sexual attention.
  - Advocating for, or encouraging, any of the above behavior.
  - Repeated harassment of others. In general, if someone asks you to stop, then stop.

- **When we disagree, try to understand why.** Disagreements, both social and technical, happen all the time and CardsChat is no exception. It is important that we resolve disagreements and differing views constructively. Remember that we're different. The strength of CardsChat tribe comes from its varied team, people from a wide range of backgrounds. Different people have different perspectives on issues. Being unable to understand why someone holds a viewpoint doesn't mean that they're wrong. Don't forget that it is human to err and blaming each other doesn't get us anywhere. Instead, focus on helping to resolve issues and learning from mistakes.

*Credits for the sources and inspiration of this code of conduct go to the [Django Project](https://www.djangoproject.com/conduct/) and many others.*
