# Local Environment   
  
## Requirements
Please see the requirements below to run the forum on your local machine.

- [Docker](#docker)
- [Bash](#bash)
- [Git](#git)
- [Git LFS](#git-lfs)  

### Docker 

This include Docker Engine, Docker CLI Client and Docker Compose packaged.

- Docker Desktop for Mac  
[https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

- Docker for Linux  
[https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

- Docker Desktop for Windows    
[https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

### Bash <small>Windows only</small>

There are few options, but the easiest is **Git Bash** which ships with **Git for Windows**.

- Git for Windows  
[gitforwindows.org](https://gitforwindows.org/) or `choco install git`
  
- WSL2  
[docs.microsoft.com/en-us/windows/wsl/wsl2-install](https://docs.microsoft.com/en-us/windows/wsl/wsl2-install)   

- CygWin  
[www.cygwin.com](https://www.cygwin.com/)  

### Git
Installation is well described here: [https://git-scm.com/download](https://git-scm.com/download)

**Mac**

- Homebrew: `brew install git`  
- or Install XCode which includes git (not the latest version though)  
- or Download and Install: [https://git-scm.com/download/mac](https://git-scm.com/download/mac)  

**Linux**  

`sudo apt update && sudo apt install git` (ubuntu)  

**Windows**

- Download and Install: [gitforwindows.org](https://gitforwindows.org/)  
- or Chocolatey: `choco install git`  


::: warning
Please make sure your git client is configured.
```bash
git config --global user.name "Firstname Lastname"
git config --global user.email "yourworkemail@workdomain.com"
```    
:::


### Git LFS
The repository configured to use GIT LFS for asset files. Installation of `git-lfs` is also described here: [https://git-lfs.github.com/](https://git-lfs.github.com/)

**Mac**  

- Download and Install: [git-lfs.github.com](git-lfs.github.com)  
- Homebrew: `brew install git-lfs`  
- MacPorts: `port install git-lfs`  

**Linux**  

- Download and Install: [git-lfs.github.com](git-lfs.github.com)  
- `sudo apt update && sudo apt-get install git-lfs` (ubuntu)  

**Windows**  

- Download and Install: [gitforwindows.org](https://gitforwindows.org/)  
- Chocolatey: `choco install git-lfs.install`



## Instructions

#### 1. Open your command line and navigate to your projects directory

This is the directory where you store your projects. This could be either `projects`, `sites`, `webprojects` or anything else you prefer.
    
#### 2. Clone the repository into the `cardschat.com-forum` directory 

(The name of this directory can be anything)
```bash
git clone git@bitbucket.org:legendcorp/cardschat.com-forum.git cardschat.com-forum
```

::: warning
Please make sure LFS is initialized (all OS).
```
$ git lfs install
> Git LFS initialized.
```
:::

#### 3. Make sure you have nothing running on `localhost` port `80`

#### 4. Create a new configuration file

There is a `conf_global.example.php` file in the root working directory. Use this file as a base and create a new 
configuration file called `conf_global.php`.

The default values are set up correct to be able to connect to your local docker database instance.  

#### 5. Spin up containers
Navigate into the `_local_environment` directory and spin up the containers.

`docker-compose up -d` for detached mode
`docker-compose up` 


> **Please Note:**
> It might take a while to download the neccessary docker images and to set up the database. 

#### 6. Log into the web container
You will have to log into the container to be able to run commands required to complete the set up.
 
`docker-compose exec cardschat-forum_web bash`  

#### 7. TBC

#### 8. TBC

#### 9. TBC

#### 10. Open your browser 
Navigate to [localhost](https://localhost/) to access the website.  
Navigate to [localhost/admin](https://localhost/admin) to access the Admin Control Panel (ACP).

#### 11. Superuser Account
A superuser account was added to the database during installation (only locally).

You can use this account to access ACP (you might have an account in the database already, however, superuser has no restrictions whatsoever).

```bash
username: superuser
password: localhost
```

## Configuration
## Troubleshooting