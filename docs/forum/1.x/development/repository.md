# Repository

The repository to our new forum can be found on BitBucket via the following link: 

[https://bitbucket.org/legendcorp/cardschat.com_forum](https://bitbucket.org/legendcorp/cardschat.com_forum)  

## Branches

There are two branches to be aware of:

First, is the `master` branch - this is current out of use.

Second is the `staging` branch - cloned by our staging environment - [staging.ccf.staging.net.management](http://staging.ccf.staging.net.management/)