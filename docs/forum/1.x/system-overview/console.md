# Console (ICC)
Invision does not come with a command line tool. The software handles all processes via the admin control panel 
(i.e.: installation of application and plugins), these processes are based on the logged in user browser session. 

**We don’t think this is the right approach**  as there are so many things that can go wrong during some of these 
longer processes in the browser which could lead to a broken website on Production. We much rather execute these 
from the command line - this way most of the manual interactions with the software can be automated and built 
into our deployment process.

We built a console command that is housed in the `management` Invision application. It was built using 
[Lumen](https://lumen.laravel.com/) and uses industry standards such as Symfony Console. By using Lumen we’re 
able to integrate some of the packages built by [Laravel](https://laravel.com/) which increases our confidence 
in long term maintainability. Lumen is already a micro-framework but we wanted to make it even more lightweight 
therefore stripped the framework and removed some of the parts that seemed unnecessary (i.e.: Views, Router).

## How It Works
You will find an `icc` file in the root working directory which requires the `shell.php` file found in 
`management` application. This script can be executed directly from your terminal. 

You should be able to see all the available commands and the parameters by running `php icc` .

::: warning
Please make sure that you are logged into a container that has all the required tools and accesses available 
(database and filesystem access and php installed).
:::

## Commands


### `application:upstall`

This command ...

:::details Signature  
`{argument}` - argument description  
`--options}` - option description  
:::

:::details Example
Run `php icc application:upstall` ...
:::

### `plugin:upstall`

This command ...

:::details Signature  
`{argument}` - argument description  
`--options}` - option description  
:::

:::details Example
Run `php icc plugin:upstall` ...
:::

### `theme:import`

This command imports theme files from `theme` folder into the database (triggers the designer mode switch off procedure).

:::details Signature  
`{ids}` - list of theme ids to import (optional)
:::

:::details Example
Run `php icc theme:import 2` to import all files of theme ID 2 from the filesystem.
:::

### `db:upsert`

This command updates and inserts (upserts) data stored in repository as database artifact. It's part of the 
deployment process ensuring that data is consistent across environments.

::: tip
Read more about how Database Artifacts work in the [Database section](./../system-overview/database.md).
:::

:::details Signature
`--database` - The database connection to upsert (optional)
:::

:::details Example
Run `php icc db:upsert` to update all tracked database objects with the data stored in the filesystem.
:::

### `db:export`

This command exports data stored in the database using the artifact's manifest file. This could be useful when you 
must roll out data change across all environments (i.e., theme color variable). 

::: tip
Read more about how Database Artifacts work in the [Database section](./../system-overview/database.md).
:::

:::details Signature
`--database` - The database connection to export (optional)
:::

:::details Example
Run `php icc db:export` after changing a color variable for a theme in ACP. You'll see the changes in artifact data 
files with `git status`.
:::
