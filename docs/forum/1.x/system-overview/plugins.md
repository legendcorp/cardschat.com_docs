# Plugins

Plugins provide a way for developers to modify or extend the behavior of applications within the IPS Community Suite. 

:::tip
Read more about Plugins in [Invision's Developer Documentation](https://invisioncommunity.com/developers/docs/development/plugins/)
:::  

## Custom Plugins

We found plugins to be the best way to modify Invision so far - this could change as we're diving into the codebase. 
This way we can keep related code in isolation and turn off/remove quickly if necessary.   


## CC - Announcement Enhancements

This plugin enhances the announcement with an additional type and theme option. 
- It adds an additional subtitle field when the `Subtile & Content` option is selected.
- It adds an additional theme option using the theme's `--theme-area_background_dark` CSS variable which is 
dynamically set from ACP. 

### Configuration

No ACP configuration options

### Database Columns

#### `core_announcements` table

- `announce_subtitle_ccannouncementenhancements` column

### Screenshots

![ModCP Edit Announcement - Type](./../assets/ccannouncementenhancements-01.png)  
![ModCP Edit Announcement - Theme](./../assets/ccannouncementenhancements-02.png)


## CC - Forum URL Rewrite

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## CC - Hand Analysis

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## CC - Member Profile Fields Unique Key

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## CC - Meta Tag Manager

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## CC - Remove Mail Validation Click Step

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## CC - Translate Api

Description of the plugin here

### Configuration

No ACP configuration options

### Database Columns

#### `xyz` table

- `xyz` column
- `xyz1` column

#### `zyx` table

- `zyx` column
- `zyx1` column

### Screenshots

![ModCP Edit Announcement - Type](http://placehold.it/1980x1024)


## 3rd Party Plugins

We do not currently have any third party plugins.

## Development

Please refer to the [development section](./../development/plugins.md) for more information.