# Directory Structure

The most important files and directories are listed below, however, there are plenty of others that are required for proper functionality.

### `_tools/`
tooling folder for unique scripts

### `_local_environment/` 
anything that is required to spin up the project locally

### `admin/` 
anything that is required for a functional admin control panel (ACP)

### `api/`
anything that is required for a functional API 

### `application/`
applications of Invision - [read more here](https://invisioncommunity.com/developers/docs/development/applications/)

### `datastore/`
cache folder of Invision

### `dev/`
files required for developer mode in Invision - [read more here](https://invisioncommunity.com/files/file/7185-developer-tools/) 

### `fonts/`
TBC...

### `oauth/`
files related to oAuth support in Invision

### `plugins/`
plugins of Invision - [read more here](https://invisioncommunity.com/developers/docs/development/plugins/)

### `system/`
core files of Invision

### `system/`
test of Codeception - [read more herr](https://codeception.com/docs/01-Introduction)

### `themes/`
themes of Invision - [read more here](https://invisioncommunity.com/4guides/themes-and-customizations/advanced-theming/)

### `uploads/`
cache and data folder of Invision

### `vendor/`
composer dependencies

### `robots.txt`
rules and directives for robots