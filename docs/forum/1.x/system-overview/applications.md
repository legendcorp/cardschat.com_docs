# Applications

Invision Community is made out of applications - these are all optional but without them there 
isn’t much that can be done with the bare skeleton of Invision. The core files are located in the `system` directory 
in the root working directory.  

:::tip
Read more about Applications in [Invision's Developer Documentation](https://invisioncommunity.com/developers/docs/development/applications/)
:::  

## Core Applications - Features

These applications are mentioned on the official website as features. 
:::tip
Read more about features on the [Official Website](https://invisioncommunity.com/features)
:::

- Forums
- Commerce - _not used_
- Pages
- Blogs - _not used_
- Gallery
- Downloads - _not used_
- Calendar

## Custom Applications

### CC

This application was created with a special purpose in mind - it aids plugins and makes them able to hook into areas 
in the codebase that would not have been possible otherwise. 

### Management

This application was created to manage the forum software as a whole. Invision with all its customisations and the 
database.  

It also comes with a command line tool - Invision Community Console (ICC) - that can be used to run various 
commands helping the development flow and deployment process.

This application isn't yet installed as an Invision Application. You won't find it in the Admin Control Panel.

::: tip
Read more about [Invision Community Console (ICC)](./console.md)
:::

## 3rd Party Applications

### Member's Country

This is an application downloaded from the official marketplace - [link to application](https://invisioncommunity.com/files/file/7807-members-country/). 

::: tip
Read more about this application in [their support forum](https://invisioncommunity.com/forums/topic/422365-members-country/)
:::

## Development

Please refer to the [development section](./../development/applications.md) for more information.