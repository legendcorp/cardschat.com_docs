# Database
Invision uses MySQL to store relational data.

## Source Version Control
The benefits of having a database under source control are quite clear; on the other hand, the implementation is 
extremely tricky. 

We're very conscious about our product's nature - the majority of the data is user-generated and therefore cannot be 
version controlled. However, this should not stop us from working towards having data integrity across our environments, 
thus making collaboration easier and increasing productivity when working with the database.

We're still in the process of figuring out the best way to create, manage and execute DDL and DML statements to keep 
schema and data up to date. The biggest concerns are the workload on master instance throughout the process and 
maintainability in the long run.

The classification of our data is possible; identifying **Metadata** and **Reference Data** is relatively easy. These 
categories of data would have to be tracked and committed to our repository. It's important to note that there could be 
exceptions, as with everything.

:::tip
Read more about [classification of data](https://blog.semarchy.com/backtobasics_data_classification).
:::

These are the tools (listed below) allowing database code to flow through a pipeline along with application code.

## Migrations
TBC… but these are DDL statements for migrations-based version control.


## Artifacts
An Artifact consists of a set of files, and it is responsible for keeping track of the data in a database object - a 
*table* in our case. It's a copy of a dataset in the database defined by a manifest file. 

It aims to be the single source of truth for the tracked data. It's immutable and idempotent; therefore, safe to get 
executed multiple times. 

### Overview
The artifacts are located `/applications/management/database/artifacts` directory. They are directories with a 
`manifest.json` file in root and a `default.json` data file in a `data` subdirectory. The directory's name is the name 
of the targeted database object - the *table* in our case.

### Folder Structure
```treeview
artifact_name/
     ├── data/
     │   ├── default.json
     │   ├── development.json
     │   ├── production.json
     │   └── test.json
     └── manifest.json
```

:::tip
`artifact_name` is the name of the targeted table in the database.
:::

### Manifest
There must be `manifest.json` file present in the root directory as it defines the dataset to be tracked by the artifact. 

This is how a simple manifest file looks like:
```json
{
  "identifiedBy": "key",
  "method": "upsert",
  "columnDefaults": {
    "mode": "track",
    "serialization": false
  },
  "columns": {
    "id": {},
    "key": {},
    "value": {},
    "some-other-value": {
      "mode": "skip"
    },
    "text-data-type-but-json": {
      "serialization": true
    }
  }
}
```

#### Properties

##### `identifiedBy` 
- the column(s) that uniquely identifies a record within the associated table
- used if the methodology is `upsert`
- define an array if multiple columns are used to identify a record
- define `false` if not used
- doesn't have to be the primary index

:::tip
Please note that records can be identified by other unique columns and not just the primary index (i.e., ID). 
In this case, however, it's worth noting that there could be a relation with a record in another table based on the 
primary index, which can break. It's therefore recommended to track the data in the related table to assure 
data integrity. 
:::

##### `method` 
- the methodology of the update
- `upsert` -  updates an existing record or create a new record if no matching record exists
- `truncateInsert` - removes all records and inserts the records from data file (use it when identification isn't 
possible)
:::danger
Please use `truncateInsert` wisely as it is a **destructive operation**. 
:::

##### `columnDefaults` 
- the default (inherited) properties for all columns
:::details Explanation and Example
Let's say you have the following manifest file:
```json
...
"columnDefaults": {
  "mode": "track"
},
"columns": {
  "id": {},
  "key": {},
  "some-other-value": {
    "mode": "skip"
  }
}
...

```

The columns will inherit all the properties automatically defined in `columnDefaults` during execution. These default 
(inherited) properties are overwritten if the column has a custom property definition. 

The previous manifest file translates to:
```json
...
"columns": {
  "id": {
    "mode": "track"  
  },
  "key": {  
    "mode": "track"
  },
  "some-other-value": {
    "mode": "skip"
  }
}
...
```
:::

##### `columns` 
- the list of the columns in the associated table
- it's a **requirement to define all columns** to assure data integrity 
- `"mode": "track"` - the content of this column is **tracked**
- `"mode": "skip"` - the content of this column is **skipped**
- `"serialization": "false"` - disable serialization
- `"serialization": "json"` - enable JSON serialization (if column stores JSON string)


### Data
There must be `default.json` file present in the `data` directory, and it must contain the dataset to be processed 
(even if it's empty).

:::details Example and Explanation
TBC....
:::

:::warning Upcoming Feature
There is a work in progress to bring support for variable substitution when working with these data files.
It will also be possible to have environment-specific data files. These files will only be merged and processed in the 
specified environment.
:::

:::danger
It's **not** recommended to use artifacts to store sensitive data (i.e., API credentials) or seedable data (test).
:::

## Seeders
TBC… but these are DML statements for temporary and test Master Data

## References
Database Version Control is difficult, and there should be more discussions about this topic. I can recommend the 
following trusted articles if you are interested in learning more about it.
- [Liquibase - Database Version Control](https://www.liquibase.com/resources/guides/database-version-control)
- [InfoQ - The Definitive Guide to Database Version Control](https://www.infoq.com/articles/Database-Version-Control/)
- [redgate - A strategy for implementing database source control](https://www.red-gate.com/hub/product-learning/sql-source-control/strategy-implementing-database-source-control)
- [CODING HORROR - Get Your Database Under Version Control](https://blog.codinghorror.com/get-your-database-under-version-control/)
- [CODING HORROR - Is Your Database Under Version Control?](https://blog.codinghorror.com/is-your-database-under-version-control/)
