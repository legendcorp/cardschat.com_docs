# Local Environment   
  
## Requirements
Please see the requirements below to run cardschat on your local machine.

- [Docker](#docker)
- [Bash](#git)
- [Git](#git)
- [Git LFS](#git-lfs)
- [CodeLibrary](#codelibrary)  

### Docker 

This include Docker Engine, Docker CLI Client and Docker Compose packaged.

**Docker Desktop for Mac**  
[https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

**Docker for Linux**  
[https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

**Docker Desktop for Windows**  
[https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

### Bash
*\*Windows only*

There are few options but the easiest is **Git Bash** which ships with **Git for Windows**.

- Git for Windows   
[gitforwindows.org](https://gitforwindows.org/) or `choco install git`
  
- WSL2  
[docs.microsoft.com/en-us/windows/wsl/wsl2-install](https://docs.microsoft.com/en-us/windows/wsl/wsl2-install)   

- CygWin  
[www.cygwin.com](https://www.cygwin.com/)  

### Git
Installation is very well described here: [https://git-scm.com/download](https://git-scm.com/download)

#### Mac
- Homebrew: `brew install git`  
- or Install XCode which includes git (not the latest version though)  
- or Download and Install: [https://git-scm.com/download/mac](https://git-scm.com/download/mac)  

#### Linux  
`sudo apt update && sudo apt install git` (ubuntu)  

#### Windows
- Download and Install: [gitforwindows.org](https://gitforwindows.org/)  
- or Chocolatey: `choco install git`  


::: warning
Please make sure your git client is configured.
```bash
git config --global user.name "Firstname Lastname"
git config --global user.email "yourworkemail@workdomain.com"
```    
:::


### Git LFS
The repository configured to use GIT LFS for asset files. Installation of `git-lfs` is also described here: [https://git-lfs.github.com/](https://git-lfs.github.com/)

**Mac**  

- Download and Install: [git-lfs.github.com](git-lfs.github.com)  
- Homebrew: `brew install git-lfs`  
- MacPorts: `port install git-lfs`  

**Linux**  

- Download and Install: [git-lfs.github.com](git-lfs.github.com)  
- `sudo apt update && sudo apt-get install git-lfs` (ubuntu)  

**Windows**  

- Download and Install: [gitforwindows.org](https://gitforwindows.org/)  
- Chocolatey: `choco install git-lfs.install`

### Read Access to CodeLibrary's Repository
[https://bitbucket.org/legendcorp/codelibrary/src/master/](https://bitbucket.org/legendcorp/codelibrary/src/master/)


## Instructions

**1. Open your command line and navigate to your projects directory**

This is the directory where you store your projects. This could be either `projects`, `sites`, `webprojects` or anything else you prefer.
    
**2. Clone the repository into the `cardschat` directory** 

(The name of this directory can be anything)
```bash
git clone git@bitbucket.org:legendcorp/cardschat.com.git cardschat
```

::: warning
Please make sure LFS is initialized (all OS).
```
$ git lfs install
> Git LFS initialized.
```
:::

**3. Point `local.cardschat.com` to you localhost**

Add the following line to your `hosts` file. You have to have elevated permissions to edit.
```bash
127.0.0.1   local.cardschat.com
```

**Mac / Linux**  
`vim /etc/hosts`

**Windows**  
`notepad c:\windows\system32\drivers\etc\hosts`

#### 4. Run the Command Line Tool

There is a CLI tool that takes care of everything so that you can start working on cardschat locally. 
The `ccli.sh` is located in the `_local_environment` directory. You can execute it by navigating into that directory, **however, I do recommend adding an alias** pointing to it from cardschat's working directory. It will make life much easier.
	
Add the following line to your `~/.bashrc` or `~/.bash_profile` or `~/.bash_aliases` (whichever is in use). 
Use an alias of your preference - I prefer `cc` 

```
alias cc=./_local_environment/ccli.sh
```

Reload your command line or source the file in order to activate the alias.
`source ~/.bash_rc` (or the file you edited)


> **Please Note:**
> The path is intentionally relative, so that the cli tool could manage multiple cardschat instances. 
> The `cc` command will **only** work from cardschat's working directory, and this is also intentional. You can, however, use absolute path if you want global access to the cli tool. 
>
> Be aware that `cc` could be pointing to a C++ Compiler on some OS. If you are using it then you might want to consider adding something else as an alias.

	
#### 5. Installation
You can then execute `cc install` command to start the process. This could take a long time so :coffee:
 or :tea: is recommended. The script will throw an error if there is something wrong. Be patient!

#### 6. Spin up containers
Entering `cc up -d` will spin up the containers if they are not running already. This command translates to `docker-compose up -d` executed from `_local_environment` directory.

#### 7. Generate up-to-date static asset files
CardsChat comes with some static asset files checked into the repository but these are old and will be removed eventually.

You can access `npm` toolkit directly which runs inside the container. This is handy as `npm` runs in an environment that is very similar to production.

Let's run it and generate the static files from `/Sources/` directory.

`cc npm run process:assets`

*Note:* Any changes you make in the `/Sources/` directory will only show up on the website after running `cc gulp`.

TODO: Implement `gulp watch` to keep an eye on file changes in `/Sources/` - avoids running `gulp` manually.  

#### 7. Open your browser 
Navigate to [local.cardschat.com](https://local.cardschat.com/) to access the website.  
Navigate to [local.cardschat.com/admincp](https://local.cardschat.com/admincp) to access the admin panel.

#### 9. Superuser Account
A superuser account was added to the database during installation (only locally).

You can use this account to access admincp (you might have an account in the database already, however, superuser has no restrictions whatsoever).

```bash
username: superuser
password: localhost
```
:::tip Cannot login?
If you cannot login, you can try adding the superuser again with the following command:

```bash
cc su add
```
:::

#### 10. Find out more about ccli
You can read more about ccli [here](#) but there is also a `-h` param which even works with subcommands.

```bash
cc -h
cc codelibrary -h
```

## TBC...

## MAMP/LAMP  Setup (DEPRECATED)
  
### Webserver Requirements  
  
Apache 2.4 or higher.  
     
Required Apache Modules:  
* ssl (enable mod_ssl)  
* rewrite (enable mod_rewrite)  
* headers (enable mod_headers)  
* expires (enable mod_expires)  
* deflate (enable mod_default)  
* filter module (enable mod_filter)  
  
### PHP Requirements  
  
PHP 7.1       
  
Required PHP extensions:  
* gd  
* zip  
* xml  
* PDO  
* mysqli  
* curl  
* intl  
* enable short open tags (**short_open_tag = On**) in the php.ini file as a lot of older pages are still using them.   
### Other Requirements  
  
* Node.js runtime and npm (Node Package Manager) - they are usually shipped together  
* gulp (js build tool)  
* **Windows users:** install Git-Bash for Windows  
* MAKE SURE MYSQL/MARIADB IS SET TO  
* character-set-server  = latin1  
* collation-server      = latin1_swedish_ci  
  
### Getting the site to run
1. Clone the site repository into the folder specified in your virtual host config.  
  
2. Clone the CodeLibrary repository into a separate folder but outside the site's root folder.   
3. Delete the entire **.git** folder within the CodeLibrary folder using the command  
         rm -Rf .git  
    Double check that the folder has actually been removed. This is to prevent you from accidentally making and committing changes  
 to this repository which might potentially break all dependent sites.  
4. Now you are going to copy the entire CodeLibrary into the site's CodeLibrary folder while **not overriding** common files that are already  
 in place.     
        cd LOCAL_WORKING/Scripts and run UpdateCodeLibrary.sh  
  5. Setup a virtual host for the site using the sample virtual host file for reference purposes but adapted to your preferences and dev environment.   
   The file is located in  
         {site-root-folder}/LOCAL_WORKING/ApacheConf/virtual-host.conf  
    Re-validate the server configuration by running one of the following commands:  
         Linux, Mac OSX: apachectl -t  
 Windows: C:\{path-to-apache-bin-folder}\httpd.exe -t    **Restart Apache for the virtual host config to be loaded.**  
  6. Locate your local hosts file  
         Linux, Mac OSX, Unix: /etc/hosts  
 Windows: c:\windows\system32\drivers\etc\hosts    Add a local dns entry for the new local domain by appending the hosts file with the following  
         127.0.0.1   local.{site-domain-name-without-wwww}  
    
7. Navigate to the site root folder and run the following commands to build static asserts and invalidate the cache.  
         npm install  
 grunt dev    
8. From within the site root, create a symlink to Games.js within the CodeLibrary in sources/Js with the following command  
         ln -s ../../CodeLibrary/Javascript/Games.js Sources/Js/games.js  
 ln -s CodeLibrary/ThirdParty script  
  9. Run the following bash script to setup the site's databases and database users  
         {site-root-folder}/LOCAL_WORKING/DatabaseScripts/setupDB.sh  
    **Windows users:** add the mysql bin folder to your systems path else this script will fail. Administrator priviledges  
 are required to alter the system environment variables. As an example here is how you do that on Windows 10:         1. Open Windows Explorer  
 2. Right click on 'This PC' and select 'properties' 3. In the window that opens, click on 'Advanced System Settings' on the left. 4. In the second window that opens, click on 'Environment Variables'        5. In the bottom half of the window that opens, labeled 'System variables', locate the variable named 'Path' and click 'Edit'  
 6. Click 'New' in the window that opened. 7. Provide the full path to the mysql bin folder and hit the 'enter' key on your keyboard then click 'OK' 8. Close all the previously opened windows. 9. Open a new Git-Bash window 10. Type 'which mysql' and Git-Bash should display the location of mysql.exe which means it is now recognized as a command. 10. The local site should be ready now to use.



## Configuration
## Troubleshooting