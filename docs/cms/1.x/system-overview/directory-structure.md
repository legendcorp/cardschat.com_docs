# Directory Structure

The most important files and directories are listed below, however, there are plenty of others that are required for proper functionality.

### `_build_tools/`
webpack and gulp task files

### `_local_environment/` 
anything that is required to spin up the project locally

### `admincp/` 
anything that requires for a functional control panel

### `attachments/`
contains files generated by forum users (constantly changing) 

### `clientscript/`
vBulletin core scripts (already minified)

### `CodeLibrary/`
CodeLibrary files

### `cpstyles/`
control panel (admincp) styles

### `customgroupicons/`
contains files generated by forum users (constantly changing)

### `customprofilepics/`
contains files generated by forum users (constantly changing)

### `dbtech/`
files required for forum URL rewrite

### `dist/`
distribution files generated by Webpack (from ### `Sources/component-library`)

### `exported_plugins/`
most of the plugins used by vBulletin

### `fonts/`
fonts used by the project

### `images/`
distribution files generated by Gulp (from ### `Sources/Images`)

### `images_fb/`
distribution files generated by Gulp (from ### `Sources/Images_fb`)

### `includes/`
core files of vBulletin (also the config - ### `includes/config.php`)

### `js/`
distribution files generated by Gulp (from ### `Sources/Js`)

### `modcp/`
anything that requires for a functional moderation panel

### `node_modules/`
node dependencies

### `pkimg/`
distribution files generated by Gulp (from ### `Sources/PKimg`)

### `Sources/`
contains most of the resource files

### `storage/`
contains protected files (downloadable after login)

### `styles/`
distribution files generated by Gulp (from ### `Sources/Css`)

### `vendor/`
composer dependencies

### `dberror.log`
contains recent vBulletin database errors (active)

### `global.php`
initialization file, firing up vBulletin

### `gulpfile.js`
configuration file of gulp related tasks

### `package.json`
configuration file of node dependencies

### `phperror.log.log`
contains recent PHP errors (active)

### `robots.txt`
rules and directives for robots

### `view.php`
view file for displaying webtemplates (CMS)

### `view2.php`
view file for displaying webtemplates (CMS)

### `view3.php`
view file for displaying webtemplates (CMS)

### `view4.php`
view file for displaying webtemplates (CMS)

### `view5.php`
view file for displaying webtemplates (CMS)

### `viewde.php`
view file for displaying webtemplates (CMS)

### `viewfr.php`
view file for displaying webtemplates (CMS)

### `viewnl.php`
view file for displaying webtemplates (CMS)