# vBulletin Overview

## General Information

[CardsChat.com](https://www.cardschat.com) runs on vBulletin CMS with many customizations. Most of the content is currently created through the vBulletin CMS. The repository is used for everything else except content and forum related templates. However, some templates are now "broken out" from the CMS and are found in the repo instead. These are marked with "File" when viewing WebTemplates - ROCKIT. A grey File means the file is present, a green File means it is in use. 
![File images](https://gyazo.com/ee832b385e4c401c50dc4fac431ed926.png)

Repository: [https://bitbucket.org/legendcorp/cardschat.com/src/master/](https://bitbucket.org/legendcorp/cardschat.com/src/master/)

CSS and JS are updated in Source directory, which writes to /styles/ and /js/ respectively. Many of the css and js files use versions, which must be updated on the AdminCP under Script Version.

## Loading new items to Logicians on CardsChat adminCP 
1. Log in to adminCP. Click WebTemplates from the Logician’s WebTemplates section               

1. Click “Add a New WebTemplate” on the top right: 

1. Fill the WebTemplate form:

    1. WebTemplate Name – This is the file name. Short, no spaces or characters. Something like “page-name”

    1. User Friendly Name – Enter the page Title here from the content. Would be like “This is the page title – This is what the page is about 2018”

    1. Category – Choose the most appropriate category. Most toplist pages can go under “New Toplists”. Reviews go under “New Reviews”, strategy goes under “BVStrat”, non-commercial articles go under “Articles”.

    1. Parse Method – leave as HTML

    1. WebTemplate Content – This is where you enter the actual html of the page. Cannot use PHP code here, must use vBulletin code – see CC shortcode cheatsheet (further down in this document) for things such as dates, etc. Do not input any header or footer information, this is simply the page content.

    1. Additional Functions – ignore

    1. Using a Theme? – Typically will use [Default] – Language, like [Default] – English or [Default] – German

    1. Header and footer – can leave as Default_vBulletin_Header and _Footer

    1. Meta Keywords – ignore

    1. Meta Description – Place the page “meta description” here

    1. Can ignore the next section – Is Draft?, Disallowed Usergroups, Disallowed Usernames, Editor Usergroups, Editor Usernames
    1. Language code and Country Code – If not English, enter the codes from the references listed on Logicians [https://www.w3schools.com/tags/ref_language_codes.asp](https://www.w3schools.com/tags/ref_language_codes.asp) and [https://www.w3schools.com/tags/ref_country_codes.asp](https://www.w3schools.com/tags/ref_country_codes.asp)

    1. Can skip the whole “Misc” section

    1. Included VB Templates – ignore

    1. PHP Include – Can enter PHP code here but use sparingly. Do enter the following though, changing to the page appropriate content:
    ```php
    $disableH1 = true;
    $topH1 = "Online Poker in Alabama";
    $pgtype = 'casino';
    $lang = 'ae';
    ```
    Where topH1 is the Main h1 for the page. If none given use the most logical part of the page title. So if page title is “Top 10 Bitcoin Poker Pages for 2018 – Best Bitcoin Poker Sites” – then the topH1 would be “Bitcoin Poker”. $pgtype – only need to enter ‘casino’ if it is a casino page, otherwise ignore pgtype and just delete this line. $lang – only enter if non-English page. If it is an English page just ignore and delete this line.
    
    Other options in phpinclude:
    ```php
    $canonical = "https://www.cardschat.com/mobile-poker.php";
    $atfFile = 'commercial';
    $atfFile = 'review';
    $atfFile = 'games';
    $atfFile = 'noncommercial';
    $yio = date('Y') - 1997;    
    $topH1disable = true;
    ```
    xvi. Overwrite Existing Webtemplate? – Leave unchecked, do not overwrite another webtemplate.

4. Now click Create WebTemplate
   
5. That will bring you back to the main Logicians screen. Now you can find your new template and check it by clicking [visit] at the end of the line. You can also click [edit] here to edit the template if needed. [remove] will delete the template and [history] will show the history of edits on the template. 

** Note that all items can be edited and publishing a new web template will not put it live immediately. We should avoid changing the WebTemplate Name, however.

## CC Shortcodes

vBulletin uses shortcodes which are like variables. They all are formatted like $variablename. Therefore you cannot use $ in any code used on the pages of CardsChat such as in scripts. Anything following a $ will be seen by the system as a variable. The only exception is if the $ is followed directly by a number or space.

**Dates:**

current year = `$copyrightyear` or `$year` both work the same

current month and year = `$monthyear`

full date = `$date`

abbreviated months = `$shortmonth`

current month in other languages = `<currentmonth lang="xx"></currentmonth>` for example for Portuguese `<currentmonth lang="pt"></currentmonth>` de `$year`

`<today locale="fr_fr"></today>` produces: 19 Avril 2015 

`<today locale="de_de"></today>` produces: 19. April 2015 

Quick CC Fact Box = `$quickfacts`

**Member Numbers**

Member count  = `$membernumber`

`$totalusers` = members - banned members

`$demembernumber` - member count with period instead of comma

`$vboptions[members_in_us]`

`$vboptions[members_in_au]`

`$vboptions[members_in_ca]`

`$vboptions[members_in_gb]`

`$totauserlonline` = number of users online currently

`$totalonline` = number of users online currently (used on homepage)

Total Posts = `$postnumber`

`$activemembers` = # of active members in past 30 days

activemembers chart - https://www.cardschat.com/view.php?pg=active-member

`$usercountries` = count of all the countries we have users from

`$totalusers[GB]`  for UNITED KINGDOM

or `$totalusers[PH]` for Philippines  

meaning:
`$totalusers[theCountryCodeYouWant]`

Total Threads / thread count = `$threadnumber`

### Languages and currency on toplists:

Ticket for language and currency:
https://alegend.basecamphq.com/projects/5148638-cardschat-com/todo_items/171946658/comments

You can tell it to use other currencies:
```php
currency="euro"
currency="pound"
currency="yen"
currency="dollar" or currency="$"
```

Now you can use:
```php
language="de" force="AT" for Austria
language="de" force="CH" for Switzerland
language="de" for Germany
language="frc" for French Canada
language="en" force="SG" for Singapore
Switzerland french - so language="fr" force="ch" 
Argentina - language="es" force="ar" 
Malaysia - language is English but force="my" 
Brazil - language="pt" force="br" 
Chile - language="es" force="cl" 
India - language is English but force="in" 
Mexico - language="es" force="mx"
Finland - language="fi"
Norway - language="no"
```
`repeattop="1"` - repeats the big box partner in the table

### Pull info from admin db:
ticket - https://basecamp.com/1779667/projects/4801610/todos/91582549
`<websiteinfo type="casino" siteid="1" info="bonus_percent"></websiteinfo>`

**type**: poker (default), casino or sportbook.

**siteid**: id of the website in the related type.

**info**: bonus_percent, bonus_amount, bonus_amount_za, bonus_code, exclusive_offers, review_link, logourl, network, affiliate_link and rating

### Forum posts on articles:
https://alegend.basecamphq.com/projects/5148638-cardschat-com/todo_items/177181689/comments#263783782

For latest posts, use something like:
`<latestposts forums="2,4" last="5"></latestposts>`

For latest threads, use something like:
`<latestthreads forums="2,4" last="3"></latestthreads>`

Similar Threads - Change the title to change the keyword and output posts related to that keyword.
`<similarthreads title="Bovada"></similarthreads>`

Related Threads shortcode
`<similarthreads title="Bovada"></similarthreads>` (only use 1 word)

### Mobile conditionals
`<if condition="$show['on_mobile']"></if>` = show to mobile

`<if condition="!$show['on_mobile']"></if>` = Hide from mobile

```php
$show['android'] --> if on android, etc.
$show['ios']
$show['tablet']
$show['ipad']
$show['iphone']
```

### Update links for international pages
add `referralcountry="ca"` to toplist example:
```html
<toplist listid="au-sites" force="au" template="4" repeattop="1" referralcountry="au"></toplist>
```
and must add the link to the website in admin

Spoofing Geo location:
`https://www.cardschat.com/us-poker-sites.php?country=US&state=NJ` for state
`https://www.cardschat.com/us-poker-sites.php?country=CA` for country

### Dev mode:

Add `?dev=true` to the end of the URL to add a dev cookie. This will circumvent the cache so you can see your changes immediately. The cookie is permanent until you delete your cookies.

## Rewrites and redirects
Redirects and rewrites are done on .htaccess and should be done by pull request. To rewrite from a logicians template to a live page use this format:

`RewriteRule ^states/alabama/$ view.php?pg=alabama [L,QSA]`
as a note view{number}.php changes per type of page... reviews are view3.php whereas strategy view2 and toplists/states etc are view5.php, bonuses are view4

Redirects are formatted like this:

```apacheconfig
RewriteRule ^old-rule$ new-full-url [R=301,L]
RewriteRule ^visa-poker-deposit.php$ /deposit/visa/ [R=301,L]
```

## Dev Server Information
The dev server can be reached at http://dev.cardschat.com and the login information is the same as it is on the live server.

**The dev server is not a direct reflection of the live server and shouldn't be treated as such.** There will be differences between live and dev, especially as we continue working on each individually. With that in mind, care must be taken not to overwrite anything on the live server with code directly from the dev server.

**Most of the development work for most pages should be done on your local server, not the dev server.** The purpose of the dev server is for final updates, pages that don't work well on the local environment and for final staging before putting updates live.

See also http://staging.ccdocs.staging.net.management/cms/1.x/development/workflow.html#gitflow-and-how-to-fail 

The best work process is as follows:

1. Ensure no one else is working on the same page/template on the dev server 

1. Copy the corresponding template from the live server to the dev server manually.
1. Make your updates to the dev server template adding comments for any changes made.
1. Provide link for QA and review.
1. Once approved, copy your changes only to the live server (don't copy all of the comments and don't copy the entire template unless you are sure there were no updates to the template in the meantime, generally just copy the updates)

Pushes to the dev server can be done on Bitbucket on the branch "develop". The flow should be similar to this:

1. Create branch off of develop like "git checkout -b my_new_branch develop
1. Do work on my_new_branch
1. When done, commit to my_new_branch
1. Checkout develop and merge my_new_branch into develop
    1. `git checkout develop`
    1. `git merge --no-ff my_new_branch`
    1. `git push origin develop`
    1. `git push origin my_new_branch`
    
We will occasionally "reset" the dev server by syncing it back up with the live server. We will announce these planned "resets" with notice so that anyone working on the dev server can get any code needed before it is reset.