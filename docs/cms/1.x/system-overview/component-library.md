# Component Library
The name is very self-descriptive, it's a library of reusable front-end components.

It gives us the ability to use SASS and next generation Javascript. It's powered by Webpack, therefore some of the 
most compelling features such as **Code Splitting, Tree Shaking, Cache Busting, Asset Versioning, Autoprefixing and Linting** 
are available out of the box. In order to utilize these features efficiently, we have to structure the source code in a 
way that Webpack understands.  


## Source Files
The source files are located in `Sources/ComponentLibrary/` folder. This is the only folder in the project 
where SASS and ES6 files should be used. The structure of this folder is quite different compared to the other 
ones in `Sources`. Some of you might recognize or find similarities in the structure to a 7-1 pattern.

The goal was not to just have another asset folder but to create an system that is modular and more sustainable 
in the future. In this folder the code is decoupled and separated into individual pieces (components) in a 
specific way.  
 
 
## Directory Structure
The contents of `Sources/ComponentLibrary`:

### `00-abstract/`
should only contain SCSS code but when it is compiled it should never output any CSS. It gathers all SASS tools and helpers used across the project.
e.g.: global mixins, placeholders, functions and such..

### `01-base/`
should contain the minimal but common/shared CSS and Javascript that must be loaded on every individual page. 
e.g: grid, or container

### `02-components/`
should contain the actual components that are being used on the website. Right now there is no further separation by the size of component but we can easily integrate Atomic Design System or something else.  
e.g.: button—-yellow, toplist, slider

### `03-layout/`
should contain everything that takes part in laying out the site or application. (the main parts of the site)
e.g.: versions of the footer, header, navigation and maybe sidebar.

### `04-templates/`
should contain code for templates that are extended by pages. Those pages could very much look the same or very similar to each other, therefore sharing the code that is common. I guess we will have to be very innovative here coming up with template names.
e.g.: casino-reviews 

### `05-pages/`
should contain the code for the individual pages. It usually builds on top of a common template. It inherits all the dependencies of a template. **This is an entry point**.
e.g.: bitcoin-casinos, blackjack

### `06-inbound/`
should contain the marketing pieces only. These are small “websites” on their own looking nothing like CardsChat but they could share dependencies and libraries with the rest. **This is an entry point**.
e.g.: ebook, podcast


## Entry Point
The main Javascript files in `05-pages/` and `06-inbound/` folders are set as entry points. These javascript files 
define the dependencies of a particular page or inbound page (marketing pieces). Webpack clevery builds out an internal 
dependency graph for an entry point figuring out which modules and libraries it depends on.

:::tip
Read more about an Entry Point in [Webpack's Official Documentation](https://webpack.js.org/concepts/#entry).
::: 

CardsChat is a traditional web application (multi-page application - MPA) that reloads the entire page and 
displays the new one when a user interacts with the web app. Every change eg. display the data or submit data 
back to server requests rendering a new page from the server in the browser. Some of the assets are 
the same and re-downloaded, when the browser lands on a new page. This gives an unique opportunity to use 
[optimization.splitChunks](https://webpack.js.org/configuration/optimization/#optimizationsplitchunks) to create 
bundles of shared application code between each page. So multi-page applications that reuse most 
of the codes/modules between entry points can greatly benefit from such technique as shared code could be cached.


## Pages vs Inbound Pages
Inbound Pages (or we could call them marketing pieces) are somewhat special in a way they look. Most of the time
they look completely different compared to normal pages, they are like mini websites. This requires new HTML markup, 
new style definitions and probably new javascript as well. These inbound pages must go in the `06-inbound/` folder. 
They can have their own unique folder structure depending on level of complexity of the page. The good thing is that 
shared code from other areas of `ComponentLibrary` can still be used without the risk of duplicating code.

  
## Recommendations
The recommended structure for a Page or an Inbound Page depends on complexity.
:::tip
A very simple and straightforward page with a single partial, would **NOT** need nested folders. In the other hand, a page 
with greater complexity and lot of customization should have nested folders separating the code into smaller pieces.   
:::
   
### Pages
```treeview
05-pages/
    └── bitcoin-casinos/
        ├── abstract/
        │   ├── _variables.scss
        │   └── _mixins.scss
        ├── assets/
        │   └── image.jpg (if only used by css or js)
        ├── partials/
        │   └── _some-partial.scss
        ├── _some-partial-maybe.scss
        ├── bitcoin-casinos.scss
        └── bitcoin-casinos.js
```

### Inbound Pages
```treeview
06-inbound/
    └── podcast/
        ├── abstract/
        │   ├── _variables.scss
        │   └── _mixins.scss
        ├── assets/
        │   └── image.jpg (if only used by css or js)
        ├── components/
        │   └── widget-one/
        │       ├── widget-one.scss
        │       └── widget-one.js
        ├── layout/
        │   └── header/
        │       ├── header.scss
        │       └── header.js
        ├── podcast.scss
        └── podcast.js
```

## Detailed Explanation
Let’s navigate to [dev.cardschat.com/deposit/bitcoin-casinos/](https://dev.cardschat.com/deposit/bitcoin-casinos/) and 
search the source code for the term `COMPONENT LIBRARY` in devtools. Hopefully, everything works fine and you'll find a 
couple of references to the term as well as some html tags that are included by the `EntrypointAssetManager` class.

You should see something similar to the following:

HTML link tags in `<head>`:
```html
<!-- COMPONENT LIBRARY LINK TAGS -->
<link rel="stylesheet" href="https://dev.cardschat.com/dist/pages/xyz~pages/zyx~pages/bitcoin-casinos-~b1a1f2f4.css">
<link rel="stylesheet" href="https://dev.cardschat.com/dist/pages/bitcoin-casinos/bitcoin-casinos.css">
<!-- END: COMPONENT LIBRARY LINK TAGS -->
``` 

HTML script tags towards the very end of `</body>`:
```html
<!-- COMPONENT LIBRARY SCRIPT TAGS -->
<script src="/dist/runtime.js"></script>
<script src="/dist/pages/xyz~pages/zyx~pages/android-~b1a1f2f4.js"></script>
<script src="/dist/pages/bitcoin-casinos/bitcoin-casinos.js"></script>
<!-- END: COMPONENT LIBRARY SCRIPT TAGS -->
``` 

This page is using a WebTemplate named  `bitcoin-casinos`. There is an entry point called `bitcoin-casinos.js` created in
`05-pages/bitcoin-casinos` folder and it defines all dependencies of the page. Webpack will take care of including and 
sorting out the code in an effective way. There is no need for adding HTML tags to include any of the css or js files 
from the distribution folder. 

During the build process, all of dependencies are being processed and copied over to the distribution folder. The distrobution
files can be found in the `dist/` folder but these should never be referenced directly as the hashes will change. The 
processed files are automatically included during page load, Webpack and the `EntrypointAssetManager` class takes 
care of this.

## Implementation
In case the page being worked on does not require vBulletin (usually the case with Inbound Pages), the following code
can be used to spin up an `EntrypointAssetManager` instance, which then can be used to include the HTML tags.

```php
<?php
/**
 * Autoload Composer Dependencies
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

/**
 * Load Entrypoint Asset Manager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/exported_plugins/component_library_entrypoint_asset_manager_init_startup.php');

/**
 * Initialize EntrypointAssetManager
 * @param string $initialEntrypoint - the name of the entry point
 * @param string $rootPath - path to root folder of the project
 * @param array $options - custom options (optional) 
 *  'entrypointFolder' - specify 'inbound' - Defaults to 'pages'
 *  'distFolder' - specify the distribution folder - Defaults to 'dist/'
 */
if (class_exists('EntrypointAssetManager')) {
    $clEntrypointAssetManager = (new EntrypointAssetManager('name-of-entry-point', $_SERVER['DOCUMENT_ROOT'], ['entrypointFolder' => 'inbound']))->toArray();
} 
```

Use the following code to include the HTML link tags in the `<head>` tag 
```php
<!-- COMPONENT LIBRARY LINK TAGS -->
<?php echo $clEntrypointAssetManager['link_tags']; ?>
<!-- END: COMPONENT LIBRARY LINK TAGS -->
```

Use the following code to include the HTML script tags near the end of `<body>` tag
```php
<!-- COMPONENT LIBRARY SCRIPT TAGS -->
<?php  echo $clEntrypointAssetManager['script_tags']; ?>
<!-- END: COMPONENT LIBRARY SCRIPT TAGS -->
```


## Usage of Resource Files
### Stylesheets
The entry point could import a `.scss` file, this will be extracted out of javascript by Webpack during build 
time and a separate (hashed) `.css` file will be loaded by the page.

:::tip Example
This will import the file from the very same directory
```js
import './bitcoin-casinos.scss'
``` 
:::


### Images
The entry point itself (`.js` file) or an other imported (`.scss` or `.js` file) could also import OR link to an 
image which will be extracted, optimised and hashed accordingly. 

The image will be processed by Webpack, copied to the distribution folder and the path will be resolved 
and rewritten. The final `.css` or `.js` file in the distribution folder will contain the correct path 
to the final image. It's recommended to create an assets folder just to keep it nice and tidy.
 
 
:::tip Example - Javascript
Working with an image in `.js` file
```js
import SomeImage from'./assets/some-image.png';
```
Accessing the final path of an image (in dist folder)
```js
console.log(SomeImage.src)
```
:::  

:::tip Example - Stylesheet
Working with an image in `.scss` file
```scss
background: url('./assets/some-image.png')
```
:::   

### Javascript
If you import another javascript file from the entry point, such as a template or a component. That javascript file 
will be processed the very same way as described above. These javascript files should be ES Modules and imported 
in a modern way.

:::tip Example
```js
import click from './click.js';
```
:::   

This also means that each of these modules will have their own dependencies (assets) processed and sorted/optimized 
the best way it’s possible by Webpack.

You can also add normal Javascript code (not just imports) in the entry point files and it will be loaded only on 
that single page.

::: tip Example
Writing `console.log('Hello!')` to `05-pages/bitcoin-casinos/bitcoin-casinos.js` will output `Hello!` in the Chrome’s 
console when `bitcoin-casinos` page is loaded.
:::

::: tip Example
Writing `console.log('Hello!')` in `04-templates/casino-reviews/casino-reviews.js` will output `Hello!` in the Chrome’s 
console when pages are loaded using `04-templates/casino-reviews/casino-reviews.js` template.
:::

::: warning
Don’t confuse `templates` in `ComponentLibrary` with Logician Templates. Logician Templates are pretty much pages but 
the terminology/wording is wrong.
::: 


## Reference from HTML

*This is not possible at this time

Webpack works best with Single Page Applications (SPA) built with Javascript. CardsChat is not that and we doubt it 
will every be, it is a Multi Page Application (MPA) built with PHP. 

In an SPA world, HTML generation is done by Javascript and as we could see Webpack can easily resolve references to 
asset files in there.

Unfortunately, there is no easy solution for url resolution by Webpack in MPAs. Therefore, referencing asset files in 
HTML cannot be done if the asset file is located in Component Library, it'll have to be moved outside. When a `<img>` 
html tag is used, the `src` attribute must point to the image in the appropriate distribution folder (not in `Sources/`).

The `ComponentLibrary` folder should not be used to store assets that are referenced from HTML. Versioning is 
enabled for all asset files in `ComponentLibrary`, the filenames will change and therefore is not recommended 
to reference them (hard-code the path) from HTML.

:::tip Example
`another-image.jpg` should be placed in `Sources/PKimg`, which will be handled by Gulp as usual. It can then be 
referenced by `bitcoin-casinos` WebTemplate by using the following HTML tag 
```html
<img src="/pkimg/another-image.jpg">
```
:::
 

 ## Commands
 `cc npm run process:component-library` - runs only webpack to build component library without any optimization 
 
 `cc npm run build:development` runs the whole development build without any optimization
 
 `cc npm run build:production` runs the whole production build with optimizations
 
 (I'll add more if needed once these commands are set in stone)







