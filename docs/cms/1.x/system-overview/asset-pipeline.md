# Asset Pipeline
Cardschat’s asset pipeline is a mixture of [Gulp tasks](https://gulpjs.com/docs/en/getting-started/creating-tasks/) 
and [Webpack](https://webpack.js.org/concepts/) [loaders](https://webpack.js.org/loaders/) / [plugins](https://webpack.js.org/plugins/) 
working in sync. It makes it possible to work with modern web development technologies, In the other hand it also 
supports legacy code that is still in use. Gulp is mostly being used to copy and optimise images, stylesheets and 
javascript files. Most of these files include outdated, legacy code. Webpack is being used for bundling and compiling the dependencies of [Component Library](./component-library.md)’s entry points.


## Overview
First and foremost, it uses `npm` for package management and script execution, however, `npx` can also be used to run some 
of the scripts individually. The whole pipeline is separated into multiple Gulp tasks. These tasks utilise various `npm` 
packages and some, even trigger Webpack to complete a particular task.


## Runtime Environment
It is always recommended to run these tasks within the docker container. The `cc` script comes handy and provides a shorter 
alternative (an alias) to run the tasks in the container.

:::warning
- `cc` commands must be executed from the working directory (project root)
- `docker-compose` commands must be executed from `/_local_environment/` directory
:::

:::danger
It is very much possible to run the following tasks outside docker container (without the usage of `cc` and `docker-compose`) 
as long as you have `npm` and/or `gulp` installed globally on your machine. This will make it much faster 
but **it could result in an error due to version and compatibility issues. It is not recommended but it 
can reduce the build time and speed up your development.** In this case, however, you should be aware of how the commands 
are being run and what issues to look out for. **It is always recommended to run production build in the 
container** once you are happy with your changes and before creating a Pull Request.
:::


## Tasks
You'll find all the available tasks, related information and commands listed below. The commands below are listed 
in order of preference.
 
 
## Task - `styles`
- This is a task running all related subtasks in **parallel**.

:::details Commands
_Development_
1. `cc npm run process:styles` 
1. `cc npx gulp styles` 
1. `docker-compose exec cardschat_apache npm run process:styles`
1. `docker-compose exec cardschat_apache npx gulp styles`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npm run process:styles`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp styles`
:::


### Subtask - `styles:jscss`
- Executes `processStylesFromSourcesJs` function located in `/_build_tools/styles/index.js`
- Copies all `.css` files recursively from `Sources/Js` to `/js` while keeping the directory structure. 
- Uses `clean-css` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met, it is 
set to **level 2**.

:::tip
Read more about the options in [clean-css's Documentation.](https://github.com/jakubpawlowicz/clean-css#level-2-optimizations).
::: 

:::details Commands
_Development_

1. `cc npx gulp styles:jscss`  
1. `docker-compose exec cardschat_apache npx gulp styles:jscss`

_Production_

1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp styles:jscss`
:::


### Subtask - `styles:css`
- Executes `processStylesFromSourcesCss` function located in `/_build_tools/styles/index.js`
- Copies all `.css` files recursively from `Sources/Css` to `/styles` while keeping the directory structure.
- Uses `clean-css` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met, it is 
set to **level 2**.

:::tip
Read more about the options in [clean-css's Documentation.](https://github.com/jakubpawlowicz/clean-css#level-2-optimizations).
:::

:::details Commands
_Development_
1. `cc npx gulp styles:css`
1. `docker-compose exec cardschat_apache npx gulp styles:css`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp styles:css`
:::


## Task - `scripts`
- This is a task running all related subtasks in **parallel**.

:::details Commands
_Development_
1. `cc npm run process:scripts` 
1. `cc npx gulp scripts` 
1. `docker-compose exec cardschat_apache npm run process:scripts`
1. `docker-compose exec cardschat_apache npx gulp scripts`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npm run process:scripts`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp scripts`
:::

### Subtask - `scripts:js`
- Executes `processScriptsFromSourcesJs` function located in `/_build_tools/tasks/scripts.js`
- Copies all `.js` files recursively from `Sources/Js` to `/js` while keeping the directory structure.
- Uses `terser` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met.

:::details Commands
_Development_
1. `cc npx gulp scripts:js`
1. `docker-compose exec cardschat_apache npx gulp scripts:js`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp scripts:js`
:::


### Subtask - `scripts:analytics`
- Executes `processAnalyticsFromSourcesJs` function located in `/_build_tools/tasks/analytics.js`
- Uses `webpack` to create `/js/datacapture.min.js` file. It bundles and compiles this script from `/Sources/Js/analytics`.

:::details Commands
_Development_
1. `cc npx gulp scripts:analytics`
1. `docker-compose exec cardschat_apache npx gulp scripts:analytics`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp scripts:analytics`
:::


## Task - `images`
- This is a task running all related subtasks in **parallel**.

:::details Commands
_Development_
1. `cc npm run process:images` 
1. `cc npx gulp images` 
1. `docker-compose exec cardschat_apache npm run process:images`
1. `docker-compose exec cardschat_apache npx gulp images`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npm run process:images`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp images`
:::

### Subtask - `images:images`
- Executes `processImagesFromSourcesImages` function located in `/_build_tools/tasks/images/index.js`
- Copies all files recursively from `Sources/Images` to `/images` while keeping the directory structure.
- Uses `imagemin` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met.

:::details Commands
_Development_
1. `cc npx gulp images:images`
1. `docker-compose exec cardschat_apache npx gulp images:images`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp images:images`
:::


### Subtask - `images:images_fb`
- Executes `processImagesFromSourcesImagesFb` function located in `/_build_tools/tasks/images/index.js`
- Copies all files recursively from `Sources/Images_fb` to `/images_fb` while keeping the directory structure.
- Uses `imagemin` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met.

:::details Commands
_Development_
1. `cc npx gulp images:images_fb`
1. `docker-compose exec cardschat_apache npx gulp images:images_fb`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp images:images_fb`
:::


### Subtask - `images:pkimg`
- Executes `processImagesFromSourcesPKimg` function located in `/_build_tools/tasks/images/index.js`
- Copies all files recursively from `Sources/PKimg` to `/pkimg` while keeping the directory structure.
- Uses `imagemin` for optimisation which runs only if `NODE_ENV == 'production'` conditional is met.

:::details Commands
_Development_
1. `cc npx gulp images:pkimg`
1. `docker-compose exec cardschat_apache npx gulp images:pkimg`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp images:pkimg`
:::


## Task - `component-library`
- Executes `processComponentLibraryFromSources` function located in `/_build_tools/component-library/images/index.js`
- Uses `webpack` to create the files in `/dist` directory. It bundles and compiles these files from `/Sources/component-library`, 
more particularly it creates a dependency graph of the entry points.

::: tip
Read more about how this works in [Component Library's Documentation](./component-library.md).
:::  

:::details Commands
_Development_
1. `cc npm run process:component-library` 
1. `cc npx gulp component-library` 
1. `docker-compose exec cardschat_apache npm run process:component-library`
1. `docker-compose exec cardschat_apache npx gulp component-library`

_Production_
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npm run process:component-library`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp component-library`
:::


## Task - `build` - `default`
- This is the main and default task, it runs all of the tasks above in **series**.

:::details Commands
_Development_
1. `cc npm run build:development` 
1. `cc npx gulp build`
1. `docker-compose exec cardschat_apache npm run build:development`
1. `docker-compose exec cardschat_apache npx gulp build`

_Production_
1. `cc npm run build:production`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npm run build`
1. `docker-compose exec --env NODE_ENV=production cardschat_apache npx gulp build`
:::


## Troubleshooting

### Build Errors
Most of the build errors are caused due to version and compatibility issues. 
 - Try reinstalling npm packages - `rm -Rf node_modules` and then `cc npm install`. 

:::tip Explanation
Let's say you install the packages with `npm install` (note missing `cc`), this will work fine but it's very much
possible that you don't have the same `node` and `npm` version like the production server has. The same configuration 
might not work for the different versions of packages either, so you could run into some problems such us unknown 
properties, functions and etc... 

The next time there is an update to `packages.json` you decide to run `cc npm install`. This could create a big mess 
because all of the sudden you running the installation with a different verion of `npm` and `node` within the container.
:::

:::tip Recommendation
The best thing to do is to always install packages via the `cc npm install` command.
:::


### ModuleNotFoundError
`ModuleNotFoundError: Module not found: Error: Can't resolve 'xxyyzz' in '/path/to/file'`

The referenced file requires a module that could not be found. 
 - Try running `cc npm install` as a new package might have been added to `package.json` by a recent commit.
 - Try reinstalling npm packages - `rm -Rf node_modules` and then `cc npm install`. 
 - Try updating your branch by either pulling the recent commits, rebasing to parent branch (or merging).