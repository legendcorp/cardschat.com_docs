# Workflow

Our workflow is quite unique, the repository started off by using GitFlow, however, our true way of working is naturally closer to the GitHub Workflow.

## GitFlow - and how to fail

The preferred git workflow by the company is GitFlow - [read more here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) - 
however, as you might noticed this is not working out for projects like cardschat. It's clear that substantial changes 
must be made to adapt to GitFlow or different workflow must be adopted.  

The reason behind all of this is that our development environment is not identical with production due to 
fact that number of changes must be done in the database. Therefore doing development on the `develop` branch 
is kinda counter-productive. Developing on this branch might makes a developer to write code that is not 
required on Production at all. It could be the other way around as well, it's possible that Production 
might needs additional code to achieve the same functionality.

In the past, when changes were made on `develop` branch the commits were not cherry-picked to `master`. 
These changes were added again in new commits to `master`, this made the two branch to diverge and impossible
to compare. There is constant effort being made to keep this divergence low as possible but eventually `develop` 
branch must be reset.  

We failed this workflow, because developers realizing the above tend to work on the `master` branch 
(providing the task is relatively small) for obvious reasons - not to blame them. This type of workflow
is naturally closer to the GitHub Workflow - [read more](https://guides.github.com/introduction/flow/)

## Merging Strategies

### Branch out from `master`

Most of that changes could be done on the `master` branch as long as the change is relatively small 
and tested on local environment.


**How to do it?** 
- branch out from `master` (see our standards)
- make the change
- run local environment to test
- commit the changes (see our standards)
- push branch to origin
- create the PR (have `master` as the target branch)

At this time, this a completely fine procedure, there is no proper way of determining the size of the task.
It's completely up to the developer to determine whether a particular task can be done on `master` or 
should be done on `develop` first. It depends on how comfortable the developer making the change 
on `master`. Recommendations can be made, however, by other developers during the review process.

::: tip
As a developer, you can always clone the branch being reviewed to properly test on your local environment, 
you don't need the changes to be present in `develop` branch.
:::   

### Branch out from `develop`

There will be changes that must be done on the `develop` branch, this might include, but not limited to:
- changes that must be tested by other squad members (can't setup local and clone branch)
- changes that you don't feel comfortable doing on `master`
- substantial changes that might affect core or other major areas of the project
- features which multiple developers working on and should not be live until agreed


**How to do it?**
- branch out from `develop` (see our standards)
- make the change
- run local environment to test
- commit the changes (see our standards)
- push branch to origin
- create the PR (have `develop` as the target branch)


**How to move it to `master`?**
- branch out from `master` (using the same branch name as before but suffixing `master` e.g.: `xyz-master`)
- cherry-pick the commits that you would like to push live
- test it on your local - to make sure that you have right commits (changes) in your branch
- push branch to origin
- create the PR (have `master` as the target branch)  

This is the preferred procedure of moving the changes live that were originally added to dev. This ensures 
that there will be less conflicting code between `master` and `develop`. The code pushed to `master` exactly matches 
with the code in `develop`. It does not introduce new commits into the repository so the branch will not diverge even
further. It keeps the repository size low due to branches having common history.