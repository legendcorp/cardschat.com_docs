# Repository

Our repository can be found on BitBucket via the following link: 

[https://bitbucket.org/legendcorp/cardschat.com](https://bitbucket.org/legendcorp/cardschat.com)

It uses GIT LFS for various file types (mostly binary) that is configured via `.gitattributes`.  

## Branches

There are two branches to be aware of:

First, is the `master` branch - it's cloned by our live environment - [www.cardschat.com](https://www.cardschat.com), 
this is our **PRODUCTION** branch but it is considered to be our main **DEVELOPMENT** branch as well ([see why](./workflow.md)).

Second is the `develop` branch - it's cloned by our dev environment - [dev.cardschat.com](https://dev.cardschat.com)
