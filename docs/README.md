---
editLink: false
helpfulVotes: false
home: true
---

# Docs

This landing page splits off into the [Handbook](/1.x/) and documentation for [News](/news/1.x/), [CMS & Forum](/cms/1.x/), [Forum](/forum/1.x/), [Free Slots](/free-slots/1.x/), and the [Docs](/docs/1.x/) (this site).

## Our Values


<div class="flex flex-wrap -mx-2 -mb-4">
    <div class="w-1/2 px-2 mb-4"> 
        <LinkPanel title="Win As One" 
            subtitle="Gambling is in our soul and we are here to beat the odds but we can't do it alone. we put our hands up to help, and our hearts out to each other. We share in our success together" 
            link="#" 
            icon="/icons/ic-win-as-one.svg" 
        />
    </div>
    <div class="w-1/2 px-2 mb-4">
        <LinkPanel title="Grow Everyday" 
            subtitle="Our company keeps growing when each of us gets better at what we do. We invite feedback and speak up when we see things to improve. If we each raise the bar there's no limit to how high we can go." 
            link="#" 
            icon="/icons/ic-grow.svg" 
        />
    </div>
    <div class="w-1/2 px-2 mb-4">    
        <LinkPanel title="Shipping Greatness" 
            subtitle="World class work isn't only about getting shit done - we want quality that we're all proud of. We take risks, get the detail right and deliver on time." 
            link="#" 
            icon="/icons/ic-rocket.svg"
        />
    </div>
    <div class="w-1/2 px-2 mb-4">
        <LinkPanel title="Rocking it!" 
            subtitle="Having fun makes our company a great place to work and fun is the fuel for our creativity. We believe that happiness is contagious. It's the joy inside each of us that inspires the positivity of our unique culture." 
            link="#" 
            icon="/icons/ic-rock-it.svg"
        />
    </div>
</div>

## Repositories

<div class="flex flex-wrap -mx-2 -mb-4">
    <div class="w-1/2 px-2 mb-4"> 
        <LinkPanel title="News" 
            subtitle="legendcorp/cardschat.com_news" 
            link="https://bitbucket.org/legendcorp/cardschat.com_news" 
            :repo="'bitbucket'" 
        />
    </div>
    <div class="w-1/2 px-2 mb-4">
        <LinkPanel title="CMS & Forum" 
            subtitle="legendcorp/cardschat.com" 
            link="https://bitbucket.org/legendcorp/cardschat.com" 
            :repo="'bitbucket'" 
        />
    </div>
    <div class="w-1/2 px-2 mb-4">    
        <LinkPanel title="Forum" 
            subtitle="legendcorp/cardschat.com_forum" 
            link="https://bitbucket.org/legendcorp/cardschat.com_forum" 
            :repo="'bitbucket'" 
        />
    </div>
    <div class="w-1/2 px-2 mb-4">
        <LinkPanel title="Free Slots"
            subtitle="legendcorp/cc-free-slots"
            link="https://bitbucket.org/legendcorp/cc-free-slots"
            :repo="'bitbucket'"
        />
    </div>
    <div class="w-1/2 px-2 mb-4">
        <LinkPanel title="Docs" 
            subtitle="legendcorp/cardschat.com_docs" 
            link="https://bitbucket.org/legendcorp/cardschat.com_docs" 
            :repo="'bitbucket'" 
        />
    </div>
</div>

## Explore Popular Resources

<div class="flex flex-wrap">
    <div class="w-1/2">
        <IconLink title="Tribe Information"
            subtitle="Our Knowledge Base."
            link="https://confluence.jira.tech/pages/viewpage.action?pageId=77466869"
            icon="/icons/icon-book.svg"
            icon-size="large"
        />
    </div>
    <div class="w-1/2">
        <IconLink title="RocketChat"
            subtitle="Meet the tribe."
            link="https://confluence.jira.tech/pages/viewpage.action?pageId=77467209"
            icon="/icons/icon-rocketchat.svg"
            icon-size="large"
        />
    </div>
</div>
