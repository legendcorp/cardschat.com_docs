module.exports = {
  theme: "craftdocs",
  base: "/",
  plugins: [
    [
      "@vuepress/active-header-links",
      {
        sidebarLinkSelector: ".right-bar .sidebar-link",
        headerAnchorSelector: ".header-anchor"
      }
    ],
    [
      "vuepress-plugin-medium-zoom",
      {
        selector: ".theme-default-content img:not(.no-zoom)",
        delay: 1000,
        options: {
          margin: 24,
          background: "var(--medium-zoom-overlay-color)",
          scrollOffset: 0
        }
      }
    ],
    [
      "vuepress-plugin-container",
      {
        type: "tip",
        defaultTitle: ""
      }
    ],
    [
      "vuepress-plugin-container",
      {
        type: "warning",
        defaultTitle: ""
      }
    ],
    [
      "vuepress-plugin-container",
      {
        type: "danger",
        defaultTitle: ""
      }
    ],
    [
      "vuepress-plugin-container",
      {
        type: "details",
        before: info =>
          `<details class="custom-block details">${
            info ? `<summary>${info}</summary>` : ""
          }\n`,
        after: () => "</details>\n"
      }
    ]
  ],
  shouldPrefetch: () => false,
  head: require("./head"),
  themeConfig: {
    title: "CardsChat Docs",
    docSets: [
      require("./sets/handbook"),
      require("./sets/news"),
      require("./sets/cms"),
      require("./sets/forum"),
      require("./sets/free-slots"),
      require("./sets/docs")
    ],
    docsRepo: "legendcorp/cardschat.com_docs",
    docsDir: "docs",
    docsBranch: "master",
    searchPlaceholder:
      "Search CardsChat Docs (Press “/” to focus)",
    editLinks: true,
    nextLinks: true,
    prevLinks: true,
    searchMaxSuggestions: 10,
    codeLanguages: {
      twig: "Twig",
      php: "PHP",
      js: "JavaScript",
      scss: "SCSS",
      css: "CSS",
      json: "JSON",
      xml: "XML",
      treeview: "Folder",
      csv: "CSV"
    },
    feedback: {
      helpful: "Was this page helpful?",
      thanks: "Thanks for your feedback.",
      more: "Give More Feedback →"
    }
  },
  markdown: {
    anchor: {
      level: [2, 3, 4]
    },
    toc: {
      format(content) {
        return content.replace(/[_`]/g, "");
      }
    },
    extendMarkdown(md) {
      // provide our own highlight.js to customize Prism setup
      md.options.highlight = require("./theme/highlight");
      // add markdown extensions
      md.use(require("./theme/util/replace-anchor-prefixes").replacePrefixes)
        .use(require("./theme/markup"))
        .use(require("markdown-it-deflist"))
        .use(require("markdown-it-imsize"));
    }
  },
  postcss: {
    plugins: require("../../postcss.config.js").plugins
  }
};
