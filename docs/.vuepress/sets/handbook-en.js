module.exports = {
    selectText: "Language",
    label: "English",
    editLinkText: "Edit this page on BitBucket",
    searchPlaceholder: "Search the Handbook (Press “/” to focus)",
    sidebar: {
        "1.x": {
            "/": [
                {
                    title: "Introduction",
                    collapsable: false,
                    children: [
                        "",
                        "code-of-conduct"
                    ]
                },
                {
                    title: "Onboarding",
                    collapsable: false,
                    children: [
                        "onboarding/introduction",
                        "onboarding/tech",
                        "onboarding/popular-resources"
                    ]
                },
                {
                    title: "Architecture",
                    collapsable: false,
                    children: [
                        "architecture/high-level-schema",
                        "architecture/scenarios"
                    ]
                },
                {
                    title: "Development",
                    collapsable: false,
                    children: [
                        "development/coding-standards",
                        "development/contribution-guidelines"
                    ]
                },
                {
                    title: "Help",
                    collapsable: false,
                    children: [
                        "help/glossary",
                        "help/faq"
                    ]
                }
            ]
        }
    },
    sidebarExtra: {
        "1.x": {
            "/": [
                {
                    title: "Tribe Information",
                    icon: "/icons/icon-book.svg",
                    link: "https://confluence.jira.tech/pages/viewpage.action?pageId=77466869"
                }
            ]
        }
    }
};
