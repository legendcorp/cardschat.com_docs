module.exports = {
    title: "CardsChat Forum Documentaton | %v",
    setTitle: "Forum",
    handle: "forum",
    icon: "/icons/invision.png",
    baseDir: "forum",
    versions: [
        ["1.x", { label: "1.x" }]
    ],
    defaultVersion: "1.x",
    searchPlaceholder: "Search the Forum documentation (Press “/” to focus)",
    primarySet: true,
    sidebar: {
        "1.x": {
            "/": [
                {
                    title: "Introduction",
                    collapsable: false,
                    children: [
                        ""
                    ]
                },
                {
                    title: "Development",
                    collapsable: false,
                    children: [
                        "development/repository",
                        "development/workflow",
                        "development/applications",
                        "development/plugins",
                        "development/themes",
                        "development/tests",
                        "development/tooling",
                        "development/debugging"
                    ]
                },
                {
                    title: "Environments",
                    collapsable: false,
                    children: [
                        "environments/local-environment",
                        "environments/staging-environment",
                        "environments/production-environment"
                    ]
                },
                {
                    title: "System Overview",
                    collapsable: false,
                    children: [
                        "system-overview/invision",
                        "system-overview/directory-structure",
                        "system-overview/applications",
                        "system-overview/plugins",
                        "system-overview/themes",
                        "system-overview/console",
                        "system-overview/database",
                        "system-overview/tests",
                        "system-overview/pipeline",
                    ]
                }
            ]
        }
    },
    sidebarExtra: {
        "1.x": {
            "/": [
                {
                    title: "Tribe Information",
                    icon: "/icons/icon-book.svg",
                    link: "https://confluence.jira.tech/pages/viewpage.action?pageId=77466869"
                },
                {
                    title: "Community Squad Hub",
                    icon: "/icons/icon-book.svg",
                    link: "https://www.notion.so/winasone/Community-bbaa47a85cba471b8519a824af06fffb"
                },
                {
                    title: "Invision Developer Resources",
                    icon: "/icons/icon-book.svg",
                    link: "https://invisioncommunity.com/developers"
                }
            ]
        }
    }
};
