module.exports = {
    title: "CardsChat Docs Documentaton | %v",
    setTitle: "Docs",
    handle: "docs",
    icon: "/icons/vuepress.png",
    baseDir: "docs",
    versions: [
        ["1.x", { label: "1.x" }]
    ],
    defaultVersion: "1.x",
    searchPlaceholder: "Search the Docs documentation (Press “/” to focus)",
    primarySet: true,
    sidebar: {
        "1.x": {
            "/": [
                {
                    title: "Introduction",
                    collapsable: false,
                    children: [
                        ""
                    ]
                }
            ]
        }
    },
    sidebarExtra: {
        "1.x": {
            "/": [
                {
                    title: "Tribe Information",
                    icon: "/icons/icon-book.svg",
                    link: "https://confluence.jira.tech/pages/viewpage.action?pageId=77466869"
                }
            ]
        }
    }
};
