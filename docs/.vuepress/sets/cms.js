module.exports = {
    title: "CardsChat CMS Documentaton | %v",
    setTitle: "CMS & Forum",
    handle: "cms",
    icon: "/icons/vbulletin.png",
    baseDir: "cms",
    versions: [
        ["1.x", { label: "1.x" }]
    ],
    defaultVersion: "1.x",
    searchPlaceholder: "Search the CMS documentation (Press “/” to focus)",
    primarySet: true,
    sidebar: {
        "1.x": {
            "/": [
                {
                    title: "Introduction",
                    collapsable: false,
                    children: [
                        ""
                    ]
                },
                {
                    title: "Development",
                    collapsable: false,
                    children: [
                        "development/repository",
                        "development/workflow"
                    ]
                },
                {
                    title: "Environments",
                    collapsable: false,
                    children: [
                        "environments/local-environment",
                        "environments/dev-environment",
                        "environments/live-environment"
                    ]
                },
                {
                    title: "System Overview",
                    collapsable: false,
                    children: [
                        "system-overview/directory-structure",
                        "system-overview/vbulletin",
                        "system-overview/webtemplates-logician",
                        "system-overview/styles-templates",
                        "system-overview/template-tags",
                        "system-overview/component-library",
                        "system-overview/asset-pipeline",
                        "system-overview/internal-caching-datastore",
                        "system-overview/internal-routing",
                        "system-overview/sitemap"
                    ]
                }
            ]
        }
    },
    sidebarExtra: {
        "1.x": {
            "/": [
                {
                    title: "Tribe Information",
                    icon: "/icons/icon-book.svg",
                    link: "https://confluence.jira.tech/pages/viewpage.action?pageId=77466869"
                }
            ]
        }
    }
};
