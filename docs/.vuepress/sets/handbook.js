module.exports = {
    title: "CardsChat Handbook | %v",
    setTitle: "Handbook",
    handle: "handbook",
    icon: "/icons/icon-book.svg",
    baseDir: "",
    versions: [
        ["1.x", { label: "1.x" }]
    ],
    defaultVersion: "1.x",
    searchPlaceholder: "Search the Handbook (Press “/” to focus)",
    primarySet: true,
    locales: {
        "/": {
            lang: "en-US",
            name: "English",
            title: "CardsChat Handbook | %v",
            config: require("./handbook-en.js")
        }
    }
};
