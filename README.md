# CardsChat Docs

CardsChat Docs is a space aimed at giving Engineers a single area to document our product and technical processes.

It's written in a way that should be helpful fellow Engineers but also, with consideration of fellow, non-technical Archs.

**Url:** [staging.ccdocs.staging.net.management](http://staging.ccdocs.staging.net.management/)

## Stack

Built with [VuePress](https://vuepress.vuejs.org/), **CardsChat Docs** utilises markdown to built out simple, clean documentation.

## Installation

### 1. Clone the project
Via your command line tool, *navigate* to the root of your 'sites' directory. This will be the location you store your projects.

eg.
```bash
cd ~/Documents/Sites/
```

Next, *paste* the following command and *press* `enter`.

```bash
git clone git@bitbucket.org:legendcorp/cardschat.com_docs.git
```

### 2. Navigate to project
Via your command line tool, *navigate* into the root of the project.
```bash
cd cardschat.com_docs
```

### 3. Install the project
In order to install the project, you will require `npm`.

`npm` is a node package manager. You must ensure you have both `Node.js` and `npm` installed in order to install the project.

For instructions on how to install these if they're missing, *go to* [npmjs.com/get-npm](https://www.npmjs.com/get-npm).

#### How to check that you have `node.js` and `npm` installed
##### NodeJS
To check if you have `node.js` installed, *run* the following command:
```bash
node -v
```

It is installed if a version number is returned. For example:
```bash
v14.5.0
```

##### NPM
To check if you have `npm` installed, *run* the following command:
```
npm -v
```

It is installed if a version number is returned. For example:
```bash
6.14.5
```

### Installing the app
*Run* the following command in your project directory's root:
```bash
npm install
```

This may take a few minutes, but you'll know when it's complete.

## Running the application locally

From the root of the project, *run* the following command:

```bash
npm run docs:dev
```

This will compile everything, start up a local server ([http://localhost:8080/](http://localhost:8080/)), and watch for changes. 

There are other commands available which are yet to be documented but can be found in the [package.json](./package.json).

## Contributing
Pull requests are welcome.

All contributors should follow our [Contribution Guidelines](/docs/1.x/development/contribution-guidelines.md) and [Coding Standards](/docs/1.x/development/coding-standards.md).