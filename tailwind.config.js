module.exports = {
  purge: {
    enabled: process.env.MY_ENV_VAR === "production" ? true : false,
    content: ["./docs/.vuepress/theme/**/*.vue", "./docs/**/*.md"]
  },
  theme: {
    extend: {
      fontFamily: {
        sans: ["Jost", "GT Eesti", "Futura Std", "Futura", "Helvetica", "sans-serif"]
      },
      colors: {
        slate: "#2d3748",
        soft: "#f1f5fd",
        softer: "#fafbfe",
        blue: "#0076b2",
        red: "#da5a47",
        cinder: "#131119",
        "light-slate": "#718096"
      },
      width: {
        80: "20rem"
      },
      screens: {
        xxl: "1408px"
      }
    }
  },
  variants: {},
  plugins: []
};
